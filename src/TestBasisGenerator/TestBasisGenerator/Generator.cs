﻿using System;
using System.IO;
using System.Linq;

namespace TestBasisGenerator
{
    static class Generator
    {
        public static void Etalon(TestSurface testsurface, float xmin, float xmax, float ymin, float ymax, int pointnum, string filenamedat)
        {
            float iteratorx;
            float iteratory;

            if(File.Exists(filenamedat))//Проверка на существование файла
            {
                File.Delete(filenamedat);
            }

            StreamWriter swe = new StreamWriter(filenamedat);

            iteratorx = (xmax - xmin) / pointnum;
            iteratory = (ymax - ymin) / pointnum;

            for (float x = xmin; x <= xmax; x = x + iteratorx)
            {
                for (float y = ymin; y <= ymax; y = y + iteratory)
                {
                    swe.WriteLine("v " + x + " " + y + " " + testsurface.F(x, y));
                }
            }

            swe.Close();
        }

        public static void NeEtalon(TestSurface testsurface, float xmin, float xmax, float ymin, float ymax, int pointnum, string filenameobj)
        {
            float iteratorx;
            float iteratory;
            float xcoord1;
            float ycoord1;
            float xcoord2;
            float ycoord2;

            iteratorx = (xmax - xmin) / pointnum;
            iteratory = (ymax - ymin) / pointnum;

            Random rnd = new Random();
            StreamWriter swne = new StreamWriter(filenameobj);

            for (float x = xmin; x <= xmax - iteratorx; x = x + iteratorx)
            {
                for (float y = ymin; y <= ymax - iteratory; y = y + iteratory)
                {
                    do
                    {
                        xcoord1 = 0.8f * iteratorx * (float)rnd.NextDouble() + x + 0.1f * iteratorx;
                        xcoord2 = 0.8f * iteratorx * (float)rnd.NextDouble() + x + 0.1f;
                    }
                    while (Math.Abs(xcoord1 - xcoord2) < 0.1 * iteratorx);

                    do
                    {
                        ycoord1 = 0.8f * iteratory * (float)rnd.NextDouble() + y + 0.1f * iteratory;
                        ycoord2 = 0.8f * iteratory * (float)rnd.NextDouble() + y + 0.1f * iteratory;
                    }
                    while (Math.Abs(ycoord1 - ycoord2) < 0.1 * iteratory);

                    swne.WriteLine("v " + xcoord1 + " " + ycoord1 + " " + testsurface.F(xcoord1, ycoord1));

                    swne.WriteLine("v " + xcoord2 + " " + ycoord2 + " " + testsurface.F(xcoord2, ycoord2));
                }
            }

            swne.Close();
        }


        public static void Create(TestSurface testsurface, float xmin, float xmax, float ymin, float ymax, int pointnum_obj, int pointnum_dat, string testname)
        {
            string filenamedat = "test.dat";
            string filenameobj = "test.obj";

            float dx = (xmax - xmin) * 0.0625f;
            float dy = (ymax - ymin) * 0.0625f;

            Etalon(testsurface, xmin + dx, xmax - dx, ymin + dy, ymax - dy, pointnum_dat, filenamedat);
            NeEtalon(testsurface, xmin, xmax, ymin, ymax, pointnum_obj, filenameobj);

            string pathtodelete = "\\src\\TestBasisGenerator\\TestBasisGenerator\\bin\\Debug";
            string currentdirectory = Directory.GetCurrentDirectory() + "\\";
            string parentdirectory = Directory.GetCurrentDirectory().Remove(Directory.GetCurrentDirectory().Count() - pathtodelete.Count()) + "\\tests\\test basis\\";
            string texturefiledirectory = parentdirectory + "test.png";
            string destinationdirectory = parentdirectory + testname + "\\";

            while (Directory.Exists(parentdirectory + testname))//Проверка на существование директории
            {
                testname = testname + " new";
                destinationdirectory = parentdirectory + testname + "\\";
            }

            Directory.CreateDirectory(parentdirectory + testname);

            File.Move(currentdirectory + filenameobj, destinationdirectory + filenameobj);
            File.Move(currentdirectory + filenamedat, destinationdirectory + filenamedat);
            File.Copy(texturefiledirectory, destinationdirectory + "test.png");
        }
    }
}
