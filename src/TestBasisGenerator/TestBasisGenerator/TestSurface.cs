﻿using PreProcessing;
using System.Collections.Generic;

namespace TestBasisGenerator
{
    class TestSurface : IInterpolator
    {
        private List<ISurfacePrimitive> SurfacePrimitiveList;

        public TestSurface(List<ISurfacePrimitive> inputlist)
        {
            SurfacePrimitiveList = new List<ISurfacePrimitive>(inputlist);
        }

        public TestSurface(ISurfacePrimitive primitive)
        {
            SurfacePrimitiveList = new List<ISurfacePrimitive>();
            SurfacePrimitiveList.Add(primitive);
        }

        public float F(float inputx, float inputy)
        {
            float z = 0;
            foreach (ISurfacePrimitive primitive in SurfacePrimitiveList)
            {
                z = z + primitive.F(inputx, inputy);
            }
            return z;
        }
    }
}
