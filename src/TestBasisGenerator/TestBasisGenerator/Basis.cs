﻿using System.Collections.Generic;
using System;


namespace TestBasisGenerator
{
    static class Basis
    {
        public static void Simple()//сгенерировать все простые поверхности
        {
            int xmin = -2;
            int xmax = 2;
            int ymin = -2;
            int ymax = 2;
            int objnum = 16;//64
            int datnum = 200;
            TestSurface Surface;

            Surface = new TestSurface(new HalfSphereRotation(1, 1, 1, 1, 0, 0));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_sphere");
            Surface = new TestSurface(new HalfSphereShift(1, 1, 1, 1, 0, 0));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_groove");
            Surface = new TestSurface(new StepRotation(1, 1, 1, 1, 0, 0));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_cylinder");
            Surface = new TestSurface(new StepShift(1, 1, 0, 1));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_step");
            Surface = new TestSurface(new SlopeRotation(1, 1, 1, 1, 0, 0));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_conus");
            Surface = new TestSurface(new SlopeShift(1, 1, -1, 1, 1, -1));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_slope");
            Surface = new TestSurface(new ParabolaRotation(1, 1, 1, 1, 0, 0));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_parabola");
            Surface = new TestSurface(new ParabolaShift(1, 1, 1, 1, 0, 0));
            Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "simple_parabolic_groove");
        }

        public static void Complex(int n)//n - количество примеров
        {
            int xmin = -15;
            int xmax = 15;
            int ymin = -15;
            int ymax = 15;
            int objnum = 16;//64
            int datnum = 200;
            TestSurface Surface;
            List<ISurfacePrimitive> SurfaceList;
            Random rnd = new Random();

            for (int i = 0; i < n;i++)
            {
                SurfaceList = new List<ISurfacePrimitive>();
                int count = rnd.Next(12) + 60;
                for (int j = 0; j < count; j++)
                {
                    float x = 20 * (float)rnd.NextDouble() - 10;
                    float y = 20 * (float)rnd.NextDouble() - 10;
                    int num = rnd.Next(3) + 2;
                    for (int k = 0; k < num; k++)
                    {
                        SurfaceList.Add(RandomPrimitive(x, y, rnd));
                    }
                }
                Surface = new TestSurface(SurfaceList);
                string zeros = "";
                if (i < 99) zeros += "0";
                if (i < 9) zeros += "0";
                Generator.Create(Surface, xmin, xmax, ymin, ymax, objnum, datnum, "complex_" + zeros + (i + 1));
            }
        }

        public static ISurfacePrimitive RandomPrimitive(float x, float y, Random rnd)
        {
            int type = rnd.Next(8) + 1;
            switch (type)
            {
                case 1: return new HalfSphereRotation(rnd.Next(4), rnd.Next(4), (float)(rnd.Next(9) - 4)/20, 1 + rnd.Next(4), x, y);
                case 2: return new HalfSphereShift(rnd.Next(4), rnd.Next(4), (float)(rnd.Next(9) - 4) / 20, 1 + rnd.Next(4), x, y);
                case 3: return new StepRotation(rnd.Next(4), rnd.Next(4), (float)(rnd.Next(9) - 4) / 20, 1 + rnd.Next(4), x, y);
                case 4: return new StepShift(rnd.Next(9)-4, rnd.Next(9)-4, rnd.Next(9)-4, (float)(rnd.Next(9) - 4) / 20);
                case 5: return new SlopeRotation(rnd.Next(4), rnd.Next(4), (float)(rnd.Next(9) - 4) / 20, rnd.Next(4), x, y);
                case 6: return new SlopeShift(rnd.Next(9) - 4, rnd.Next(9) - 4, -rnd.Next(3) - 1, rnd.Next(3) + 1, (float)(rnd.Next(9) - 4) / 20, (float)(rnd.Next(9) - 4) / 20);
                case 7: return new ParabolaRotation(rnd.Next(4), rnd.Next(4), (float)(rnd.Next(9) - 4) / 20, 1 + rnd.Next(4), x, y);
                default: return new ParabolaShift(rnd.Next(4), rnd.Next(4), (float)(rnd.Next(9) - 4) / 20, 1 + rnd.Next(4), x, y);
            }
        }
    }
}
