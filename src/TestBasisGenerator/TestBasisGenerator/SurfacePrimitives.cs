﻿using PreProcessing;
using System;

namespace TestBasisGenerator
{
    interface ISurfacePrimitive : IInterpolator
    {

    }

    class HalfSphereRotation : ISurfacePrimitive //полусфера
    {
        private float a;
        private float b;
        private float c;
        private float r;
        private float x0;
        private float y0;

        public HalfSphereRotation(float inputa, float inputb, float inputc, float inputr, float inputx0, float inputy0) // a,b,c - коэффициенты растяжения, r - радиус, x0,y0 - центр.
        {
            a = inputa*inputa;
            b = inputb*inputb;
            c = inputc;
            r = inputr*inputr;
            x0 = inputx0;
            y0 = inputy0;
        }
        public float F(float inputx, float inputy)
        {
            float z;

            if (a * (inputx - x0) * (inputx - x0) + b * (inputy - y0) * (inputy - y0) > r)
            {
                z = 0;
            }
            else
            {
                z = c * (float)Math.Sqrt(r - a * (inputx - x0) * (inputx - x0) - b * (inputy - y0) * (inputy - y0));
            }

            return z;
        }
    }

    class HalfSphereShift : ISurfacePrimitive // полукруглая борозда
    {
        private float a;
        private float b;
        private float c;
        private float r;
        private float x0;
        private float y0;

        public HalfSphereShift(float inputa, float inputb, float inputc, float inputr, float inputx0, float inputy0) // a,b - линейные коэффициенты ax+by, с - к-т глубины, r - радиус, x0,y0 - центр.
        {
            a = inputa;
            b = inputb;
            c = inputc;
            r = inputr;
            x0 = inputx0;
            y0 = inputy0;
        }

        public float F(float inputx, float inputy)
        {
            float z;

            float t = a * (inputx - x0) + b * (inputy - y0);

            if (Math.Abs(t) > r)
            {
                z = 0;
            }
            else
            {
                z = c * (float)Math.Sqrt(r * r - t * t);
            }


            return z;
        }
    }

    class StepRotation : ISurfacePrimitive //цилиндр
    {
        private float a;
        private float b;
        private float z1;
        private float r;
        private float x0;
        private float y0;

        public StepRotation(float inputa, float inputb, float inputz1, float inputr, float inputx0, float inputy0)// a,b - коэффициенты растяжения, z1 - высота цилиндра, r - радиус, x0,y0 - центр.
        {
            a = inputa*inputa;
            b = inputb*inputb;
            z1 = inputz1;
            r = inputr*inputr;
            x0 = inputx0;
            y0 = inputy0;
        }

        public float F(float inputx, float inputy)
        {
            float z;

            if (a * (inputx - x0) * (inputx - x0) + b * (inputy - y0) * (inputy - y0) > r)
            {
                z = 0;
            }
            else
            {
                z = z1;
            }

            return z;
        }

    }

    class StepShift : ISurfacePrimitive //порог
    {
        private float a;
        private float b;
        private float c;
        private float z1;

        public StepShift(float inputa, float inputb, float inputc, float inputz1) // a,b - линейные коэффициенты ax+by>=c, z1 - высота порога
        {
            a = inputa;
            b = inputb;
            c = inputc;
            z1 = inputz1;
        }

        public float F(float inputx, float inputy)
        {
            float z;

            float t = a * inputx + b * inputy;

            if (t < c)
            {
                z = 0;
            }
            else
            {
                z = z1;
            }
            return z;
        }
    }

    class SlopeRotation : ISurfacePrimitive //конус
    {
        private float a;
        private float b;
        private float c;
        private float r;
        private float x0;
        private float y0;

        public SlopeRotation(float inputa, float inputb, float inputc, float inputr, float inputx0, float inputy0) // a,b,c - коэффициенты растяжения, r - радиус, x0,y0 - центр.
        {
            a = inputa;
            b = inputb;
            c = inputc;
            r = inputr*inputr;
            x0 = inputx0;
            y0 = inputy0;
        }

        public float F(float inputx, float inputy)
        {
            float z;

            float t = a * (inputx - x0) * (inputx - x0) + b * (inputy - y0) * (inputy - y0);

            if (t > r)
            {
                z = 0;
            }
            else
            {
                z = c * (r - (float)Math.Sqrt(t));
            }

            return z;
        }
    }

    class SlopeShift : ISurfacePrimitive // плавный порог
    {
        private float a;
        private float b;
        private float c1;
        private float c2;
        private float z1;
        private float z2;

        public SlopeShift(float inputa, float inputb, float inputc1, float inputc2, float inputz1, float inputz2) //a,b - линейные коэффициенты с1<=ax+by<=c2, z1,z2 - высота порога
        {
            a = inputa;
            b = inputb;
            c1 = inputc1;
            c2 = inputc2;
            z1 = inputz1;
            z2 = inputz2;

        }

        public float F(float inputx, float inputy)
        {
            float z;

            float t = a * inputx + b * inputy;

            if (t < c1)
            {
                z = z1;
            }
            else
            {
                if (t > c2)
                {
                    z = z2;
                }
                else
                {
                    z = (z2 - z1) * (t - c1) / (c2 - c1) + z1;
                }
            }

            return z;
        }
    }

    class ParabolaRotation : ISurfacePrimitive //трёхмерная парабола
    {
        private float a;
        private float b;
        private float c;
        private float r;
        private float x0;
        private float y0;

        public ParabolaRotation(float inputa, float inputb, float inputc, float inputr, float inputx0, float inputy0) // a,b,c - коэффициенты растяжения, r - радиус, x0,y0 - центр.
        {
            a = inputa*inputa;
            b = inputb*inputb;
            c = inputc;
            r = inputr*inputr;
            x0 = inputx0;
            y0 = inputy0;
        }

        public float F(float inputx, float inputy)
        {
            float z;

            float t = a * (inputx - x0) * (inputx - x0) + b * (inputy - y0) * (inputy - y0);

            if (t > r)
            {
                z = 0;
            }
            else
            {
                z = c * (t - r);
            }

            return z;
        }
    }

    class ParabolaShift : ISurfacePrimitive // параболическая борозда
    {
        private float a;
        private float b;
        private float c;
        private float r;
        private float x0;
        private float y0;

        public ParabolaShift(float inputa, float inputb, float inputc, float inputr, float inputx0, float inputy0) // a,b - линейные коэффициенты ax+by, с - к-т глубины, r - радиус, x0,y0 - центр.
        {
            a = inputa;
            b = inputb;
            c = inputc;
            r = inputr;
            x0 = inputx0;
            y0 = inputy0;
        }

        public float F(float inputx, float inputy)
        {
            float z;

            float t = a * (inputx - x0) + b * (inputy - y0);

            if (Math.Abs(t) > r)
            {
                z = 0;
            }
            else
            {
                z = c * (r * r - t * t);
            }

            return z;
        }
    }
}
