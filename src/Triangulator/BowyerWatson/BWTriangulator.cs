﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulator.BowyerWatson
{
    public class BWTriangulator : IStrategy
    {
        public BWTriangulator()
        {

        }

        public void Execute(Model m)
        {
            m.tList = this.Triangulate(m.vList);
        }

        List<Triad> Triangulate(List<Vertex> pLst)
        {
            //pLst.Sort();
            for (int i=0; i<pLst.Count;i++)
            {
                pLst[i].ind = i;
            }
            //triangulation := empty triangle mesh data structure
            List<Triad> triangulation = new List<Triad>();

            //add super-triangle to triangulation // must be large enough to completely contain all the Vertexs in VertexList
            Vertex[] super_bound = new Vertex[3];
            super_bound[0] = new Vertex(200, 200);
            super_bound[1] = new Vertex(0, -200);
            super_bound[2] = new Vertex(-200, 200);
            Triad super_Triad = new Triad(super_bound[0], super_bound[1], super_bound[2]);
            triangulation.Add(super_Triad);

            //for each Vertex in VertexList do // add all the Vertexs one at a time to the triangulation
            foreach (Vertex p in pLst)
            {
                if (p.ind == 851)
                {
                    int a = 0;
                }
                // badTriangles := empty set
                List<Triad> badTriangles = new List<Triad>();
                // for each triangle in triangulation do // first find all the triangles that are no longer valid due to the insertion
                //  if Vertex is inside circumcircle of triangle
                //   add triangle to badTriangles
                foreach (Triad t in triangulation)
                {
                    //if (t.ab.Belongs.Count > 2 || t.bc.Belongs.Count > 2 || t.ca.Belongs.Count > 2)
                    //{
                    //    Console.WriteLine("Edge in triad" + t + "has edge with more than 2 triads.");
                    //}
                    if (t.VertexInCircle(p))
                    {
                        if ((!t.ab.IsContainPoint(p)) && (!t.bc.IsContainPoint(p)) && (!t.ca.IsContainPoint(p)))
                            badTriangles.Add(t);
                    }
                }
                // polygon := empty set
                List<IEdge> polygon = new List<IEdge>();
                // for each triangle in badTriangles do // find the boundary of the polygonal hole
                //  for each edge in triangle do
                //   if edge is not shared by any other triangles in badTriangles
                //    add edge to polygon
                for (int i = 0; i < badTriangles.Count; i++)
                {
                    Triad t = badTriangles[i];
                    bool flag = false;


                    if (t.ab.Belongs.Count() == 1)
                    {
                        polygon.Add(t.ab);
                    }
                    else foreach (Triad t1 in t.ab.Belongs)
                        {
                            if (t1.isSame(t)) continue;
                            else
                            {
                                flag = false;
                                for (int j = 0; j < badTriangles.Count; j++)
                                {
                                    if (i == j) continue;
                                    if (t1.isSame(badTriangles[j]))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag) polygon.Add(t.ab);
                            }
                        }
                    if (t.bc.Belongs.Count() == 1)
                    {
                        polygon.Add(t.bc);
                    }
                    else foreach (Triad t1 in t.bc.Belongs)
                        {
                            if (t1.isSame(t)) continue;
                            else
                            {
                                flag = false;
                                for (int j = 0; j < badTriangles.Count; j++)
                                {
                                    if (i == j) continue;
                                    if (t1.isSame(badTriangles[j]))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag) polygon.Add(t.bc);
                            }
                        }
                    if (t.ca.Belongs.Count() == 1)
                    {
                        polygon.Add(t.ca);
                    }
                    else foreach (Triad t1 in t.ca.Belongs)
                        {
                            if (t1.isSame(t)) continue;
                            else
                            {
                                flag = false;
                                for (int j = 0; j < badTriangles.Count; j++)
                                {
                                    if (i == j) continue;
                                    if (t1.isSame(badTriangles[j]))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag) polygon.Add(t.ca);
                            }
                        }
                }
                // for each triangle in badTriangles do // remove them from the data structure
                //  remove triangle from triangulation
                foreach (Triad t in badTriangles)
                {
                    t.PrepareForDestruction();
                    triangulation.Remove(t);
                }
                // for each edge in polygon do // re-triangulate the polygonal hole
                //  newTri := form a triangle from edge to Vertex
                //  add newTri to triangulation
                foreach (IEdge e in polygon)
                {
                    ////TODO rework
                    bool flag = false;
                    foreach (Triad t1 in triangulation)
                    {
                        if (t1.IsContain(e) && t1.IsContain(p))
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag) continue;
                    if (!e.IsHavingPoint(p))
                        triangulation.Add(new Triad(e, p));
                }
                //Console.WriteLine(p.ind + "-nd point is added.");
            }
            //for each triangle in triangulation // done inserting Vertexs, now clean up
            // if triangle contains a Vertex from original super-triangle
            //  remove triangle from triangulation
            for (int i = triangulation.Count - 1; i >= 0; i--)
            {
                foreach (Vertex p in super_bound)
                {
                    if (triangulation[i].IsContain(p))
                    {
                        triangulation.RemoveAt(i);
                        break;
                    }
                }
            }
            //return triangulation
            return triangulation;
        }
    }
}
