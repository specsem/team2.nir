﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulator.DivAndConq
{
    public class Edge
    {
        public uint a, b;
        public Edge(uint _a, uint _b)
        {
            a = _a;
            b = _b;
        }
    }
}
