﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulator.DivAndConq
{
    public class Triangle : IEqualityComparer<Triangle>
    {
        public int id;
        public static int count;
        public uint p1, p2, p3;
        public Triangle(uint _p1, uint _p2, uint _p3)
        {
            p1 = _p1;
            p2 = _p2;
            p3 = _p3;
            id = count++;
        }

        public bool Equals(Triangle t1, Triangle t)
        {
            if ((t1.p1 == t.p1)
                && (t1.p2 == t.p2)
                && (t1.p3 == t.p3))
            {
                return true;
            }
            else if ((t1.p1 == t.p2)
                && (t1.p2 == t.p3)
                && (t1.p3 == t.p1))
            {
                return true;
            }
            else if ((t1.p1 == t.p3)
                && (t1.p2 == t.p1)
                && (t1.p3 == t.p2))
            {
                return true;
            }
            return false;
        }

        public int GetHashCode(Triangle obj)
        {
            return id;
        }
    }
}
