﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulator.DivAndConq
{
    public class DACTruangulator : IStrategy
    {
        public DACTruangulator()
        {

        }

        public void Execute(Model m)
        {
            Delaunay d = new Delaunay(m.vList);
            d.triangulate();
            d.generateTriangles();
            List<Triangle> tl = new List<Triangle>(d.triangles.Take<Triangle>((int)d.numTriangles));
            IEnumerable<Triangle> ienum = tl.Distinct(tl[0]);

            m.vList = new List<Vertex>(d.P);
            for (int i = 0; i < m.vList.Count; i++)
            {
                m.vList[i].ind = i;
            }
            foreach (Triangle t in ienum)
            {
                m.tList.Add(new Triad(m.vList[(int)t.p1], m.vList[(int)t.p2], m.vList[(int)t.p3]));
            }
        }
    }
}
