﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulator.DivAndConq
{
    public enum Orientation
    {
        ORIENT_CW,
        ORIENT_CCW,
        ORIENT_COLLINEAR,
    };

    public class Delaunay
    {
        const uint maxNumPoints = 41000;
        const int maxNumEdges = 512 * 1024 * 3;
        const uint maxNumTriangles = maxNumEdges / 2;

        public Core.Vertex[] P;
        public TriEdge[] E;
        public uint numEdges;
        public uint totalNumPoints;

        public void printTriads()
        {
            for (int i = 0; i < numTriangles / 3; i++)
            {
                Console.WriteLine("Triad " + i + " :");
                Core.Vertex v1 = P[triangles[i].p1];
                Console.WriteLine("\t (" + P[triangles[i].p1].i + ", " + P[triangles[i].p1].j + ")");
                Console.WriteLine("\t (" + P[triangles[i].p2].i + ", " + P[triangles[i].p2].j + ")");
                Console.WriteLine("\t (" + P[triangles[i].p3].i + ", " + P[triangles[i].p3].j + ")");
            }
        }

        public const uint INVALID_EDGE = Int32.MaxValue;
        uint leftmostEdge = INVALID_EDGE, rightmostEdge = INVALID_EDGE;
        public Triangle[] triangles;
        public uint numTriangles = 0;


        // auxiliary data for generateTriangles
        uint[] bfsQueue;
        bool[] bfsVisited;

        // auxiliary data for radix sort
        //PointIJ pointsSortedByI[maxNumPoints];
        //short originalIdx[maxNumPoints];
        //short numPointsPerCoord[maxCoord], coordIdx[maxCoord];


        public Delaunay(List<Core.Vertex> l)
        {
            E = new TriEdge[maxNumEdges];
            for (int i = 0; i < maxNumEdges; i++)
            {
                E[i] = new TriEdge();
            }
            triangles = new Triangle[maxNumTriangles];
            for (int i = 0; i < maxNumTriangles; i++)
            {
                triangles[i] = new Triangle(0, 0, 0);
            }

            List<Core.Vertex> lst = new List<Core.Vertex>(l);
            lst.Sort();
            P = lst.ToArray();
            totalNumPoints = (uint)lst.Count();
            bfsQueue = new uint[maxNumEdges];
            bfsVisited = Enumerable.Repeat<bool>(false, maxNumEdges).ToArray();
        }

        // FROM GEOMETRY

        Orientation triOrientation(float x1, float y1, float x2, float y2, float x3, float y3)
        {
            float val = (y2 - y1) * (x3 - x2) - (x2 - x1) * (y3 - y2);
            if (val < 0)
                return Orientation.ORIENT_CW;
            else if (val > 0)
                return Orientation.ORIENT_CCW;
            else
                return Orientation.ORIENT_COLLINEAR;
        }

        /// Return true if point (px, py) lies strictly inside circle passing through points 1,2,3.
        /// IMPORTANT! Points 1,2,3 should be oriented counterclockwise!
        bool inCircle(float x1, float y1, float x2, float y2, float x3, float y3, float px, float py)
        {
            // reduce the computational complexity by substracting the last row of the matrix
            // ref: https://www.cs.cmu.edu/~quake/robust.html
            float p1p_x = x1 - px;
            float p1p_y = y1 - py;

            float p2p_x = x2 - px;
            float p2p_y = y2 - py;

            float p3p_x = x3 - px;
            float p3p_y = y3 - py;

            double p1p = p1p_x * p1p_x + p1p_y * p1p_y;
            double p2p = p2p_x * p2p_x + p2p_y * p2p_y;
            double p3p = p3p_x * p3p_x + p3p_y * p3p_y;

            // determinant of matrix, see paper for the reference
            double res = p1p_x * (p2p_y * p3p - p2p * p3p_y)
                              - p1p_y * (p2p_x * p3p - p2p * p3p_x)
                              + p1p * (p2p_x * p3p_y - p2p_y * p3p_x);

            //assert(std::abs(res) < std::numeric_limits < int64 >::max() / 100);

            return res < 0;
        }


        // Geometry helpers.

        /// p1,p2,p3 should be oriented counterclockwise!
        bool inCircle(uint p1, uint p2, uint p3, uint p)
        {
            return inCircle(P[p1].j, P[p1].i, P[p2].j, P[p2].i, P[p3].j, P[p3].i, P[p].j, P[p].i);
        }

        Orientation orientationPointEdge(uint p, TriEdge e)
        {
            uint orig = e.origPnt, dest = destPnt(e);
            return triOrientation(P[orig].j, P[orig].i, P[dest].j, P[dest].i, P[p].j, P[p].i);
        }

        bool isRightOf(uint p, uint e)
        {
            return orientationPointEdge(p, E[e]) == Orientation.ORIENT_CW;
        }

        bool isLeftOf(uint p, uint e)
        {
            return orientationPointEdge(p, E[e]) == Orientation.ORIENT_CCW;
        }


        // Topology helpers.

        /// Same edge but in opposite direction.
        TriEdge sym(TriEdge e)
        {
            return E[e.symEdge];
        }

        TriEdge sym(uint edgeIdx)
        {
            return E[E[edgeIdx].symEdge];
        }

        /// Destination point of an edge.
        uint destPnt(TriEdge e)
        {
            return E[e.symEdge].origPnt;
        }

        uint destPnt(uint edgeIdx)
        {
            return E[E[edgeIdx].symEdge].origPnt;
        }


        /// Creates pair of oriented edges orig->dest and dest->orig.
        uint makeEdge(uint orig, uint dest)
        {
            uint e1Idx = numEdges, e2Idx = e1Idx + 1;
            numEdges += 2;
            // Safety margin, might want to allocate bigger buffer if that's exceeded.
            // Can also add "defragmentation" for edge buffer to reduce memory usage.
            //assert(numEdges < maxNumEdges * 2 / 3);

            TriEdge e1 = E[e1Idx];
            TriEdge e2 = E[e2Idx];

            e1.origPnt = orig;
            e2.origPnt = dest;

            e1.symEdge = e2Idx;
            e2.symEdge = e1Idx;

            e1.nextCcwEdge = e1.prevCcwEdge = e1Idx;
            e2.nextCcwEdge = e2.prevCcwEdge = e2Idx;

            return e1Idx;
        }


        /// Attach newEdge to the origin of oldEdge immediately after oldEdge counterclockwise.
        void join(uint newuint, uint olduint)
        {
            TriEdge newEdge = E[newuint];
            TriEdge oldEdge = E[olduint];

            //assert(newEdge.origPnt == oldEdge.origPnt);

            newEdge.nextCcwEdge = oldEdge.nextCcwEdge;
            oldEdge.nextCcwEdge = newuint;

            E[newEdge.nextCcwEdge].prevCcwEdge = newuint;
            newEdge.prevCcwEdge = olduint;
        }

        /// Connects destination of a to origin of b. Returns e = a.dest->b.orig.
        uint connect(uint aIdx, uint bIdx)
        {
            TriEdge b = E[bIdx];

            uint cIdx = makeEdge(destPnt(aIdx), b.origPnt);
            TriEdge c = E[cIdx];
            join(cIdx, sym(aIdx).prevCcwEdge);
            join(c.symEdge, bIdx);

            //assert(destPnt(aIdx) == c.origPnt);
            //assert(b.origPnt == destPnt(c));
            //assert(destPnt(aIdx) == destPnt(sym(c)));
            //assert(b.origPnt == sym(c).origPnt);
            return cIdx;
        }


        /// Removes references to given edge from the linked list around it's origin.
        void unlink(uint eIdx)
        {
            TriEdge e = E[eIdx];
            E[e.nextCcwEdge].prevCcwEdge = e.prevCcwEdge;
            E[e.prevCcwEdge].nextCcwEdge = e.nextCcwEdge;
        }

        /// Removes given edge and its pair from the subdivision.
        void deleteEdge(uint eIdx)
        {
            unlink(eIdx);
            unlink(E[eIdx].symEdge);

            sym(eIdx).symEdge = INVALID_EDGE;
            E[eIdx].symEdge = INVALID_EDGE;
        }


        /// TODO rewrite this to c-sharp
        /// Returns number of triangles and pointer to the triangle array.
        //void getTriangles(Triangle*&t, int &num)
        //{
        //    t = triangles;
        //    num = numTriangles;
        //}


        /// Simple wrapper that calls triangulateSubset for the whole set of points.
        /// Returns the leftmost edge of the subdivision convex hull.
        public void triangulate()
        {
            if (totalNumPoints < 2)
            {
                Console.WriteLine("Cannot triangulate set of ", totalNumPoints, " points");
                return;
            }

            leftmostEdge = rightmostEdge = INVALID_EDGE;
            triangulateSubset(0, totalNumPoints, out leftmostEdge, out rightmostEdge);
        }

        /// Main divide and conquer algorithm.
        /// Each call modifies the subdivision structure and returns two edges from the convex hull.
        /// le - index of CCW convex hull edge from leftmost vertex
        /// re - index of CW convex hull edge from rightmost vertex
        void triangulateSubset(uint lIdx, uint numPoints, out uint le, out uint re)
        {
            //assert(numPoints < maxNumPoints);
            le = 1; re = 2;

            if (numPoints == 2)
            {
                uint s1 = lIdx, s2 = s1 + 1;
                le = makeEdge(s1, s2);
                re = E[le].symEdge;
            }
            else if (numPoints == 3)
            {
                uint s1 = lIdx, s2 = s1 + 1, s3 = s2 + 1;
                uint aIdx = makeEdge(s1, s2);
                uint bIdx = makeEdge(s2, s3);

                join(E[aIdx].symEdge, bIdx);  // now a.sym and b are adjacent

                Orientation pos = triOrientation(P[s1].j, P[s1].i, P[s2].j, P[s2].i, P[s3].j, P[s3].i);
                switch (pos)
                {
                    case Orientation.ORIENT_CCW:
                        connect(bIdx, aIdx);
                        le = aIdx;
                        re = E[bIdx].symEdge;
                        break;
                    case Orientation.ORIENT_CW:
                        re = connect(bIdx, aIdx);
                        le = E[re].symEdge;
                        break;
                    default:
                        // points are collinear
                        le = aIdx;
                        re = E[bIdx].symEdge;
                        break;
                }
            }
            else
            {
                uint numRight = numPoints / 2, numLeft = numPoints - numRight;
                uint lle;  // CCW convex hull edge starting at the leftmost vertex of left triangulation
                uint lre;  // CW convex hull edge starting at the rightmost vertex of left triangulation
                uint rle;  // CCW convex hull edge starting at the leftmost vertex of right triangulation
                uint rre;  // CW convex hull edge starting at the rightmost vertex of right triangulation
                triangulateSubset(lIdx + numLeft, numRight, out rle, out rre);
                triangulateSubset(lIdx, numLeft, out lle, out lre);
                mergeTriangulations(lle, lre, rle, rre, out le, out re);
            }
        }


        /// Merge phase.
        void mergeTriangulations(uint lle, uint lre, uint rle, uint rre, out uint le, out uint re)
        {
            le = 1; re = 2;
            // first, find the new base edge, the lower common tangent of left and right subdivisions
            while (true)
            {
                // move edges down across convex hull of subdivision until we find a tangent
                if (isLeftOf(E[rle].origPnt, lre))
                {
                    // best right candidate for the tangent endpoint still "below", move lre further down
                    lre = sym(lre).prevCcwEdge;
                }
                else if (isRightOf(E[lre].origPnt, rle))
                {
                    // best left candidate for the tangent endpoint still "below", move rle further down
                    rle = sym(rle).nextCcwEdge;
                }
                else
                {
                    // tangent endpoints are found
                    break;
                }
            }

            // create "base" edge, we will work up from it merging the triangulations
            uint base1 = connect(E[rle].symEdge, lre);
            //assert(!isLeftOf(E[lle].origPnt, base));
            //assert(!isLeftOf(E[rre].origPnt, base));

            // it may be required to update le and re
            if (E[lle].origPnt == destPnt(base1))
                lle = E[base1].symEdge;
            if (E[rre].origPnt == E[base1].origPnt)
                rre = base1;

            le = lle; re = rre;

            // Main merging code. Find left and right candidate and update base edge to move up.
            // When no candidates are left we reached the convex hull.
            // For more information read: http://www.sccg.sk/~samuelcik/dgs/quad_edge.pdf page 114.
            while (true)
            {
                uint lCand = sym(base1).nextCcwEdge;
                bool lCandFound = false;
                while (isRightOf(destPnt(lCand), base1))
                {
                    uint nextLCand = E[lCand].nextCcwEdge;
                    if (inCircle(destPnt(base1), E[base1].origPnt, destPnt(lCand), destPnt(nextLCand)))
                    {
                        //assert(isRightOf(destPnt(nextLCand), base1));
                        deleteEdge(lCand);
                        lCand = nextLCand;
                    }
                    else
                    {
                        // left candidate is found, we can move on
                        lCandFound = true;
                        break;
                    }
                }

                uint rCand = E[base1].prevCcwEdge;
                bool rCandFound = false;
                while (isRightOf(destPnt(rCand), base1))
                {
                    uint nextRCand = E[rCand].prevCcwEdge;
                    if (inCircle(destPnt(base1), E[base1].origPnt, destPnt(rCand), destPnt(nextRCand)))
                    {
                        deleteEdge(rCand);
                        rCand = nextRCand;
                    }
                    else
                    {
                        // right candidate is found, we can move on
                        rCandFound = true;
                        break;
                    }
                }

                if (!lCandFound && !rCandFound)
                {
                    // cannot continue updating base edge, this should mean that we've reached upper common tangent of two subdivisions
                    {
                        // all triangulation should now be to the left of the base edge, let's do a couple of checks just in case
                        //assert(!isRightOf(E[lle].origPnt, base1));
                        //assert(!isRightOf(E[lre].origPnt, base1));
                        //assert(!isRightOf(E[rle].origPnt, base1));
                        //assert(!isRightOf(E[rre].origPnt, base1));
                    }

                    break;
                }
                else  // at least one valid candidate is found
                {
                    if (!lCandFound || (rCandFound && inCircle(destPnt(base1), E[base1].origPnt, destPnt(lCand), destPnt(rCand))))
                    {
                        // either lCand not found or rCand destination is in lCand triangle circumcircle --> select rCand
                        base1 = connect(rCand, E[base1].symEdge);
                    }
                    else
                    {
                        // select lCand
                        base1 = connect(E[base1].symEdge, E[lCand].symEdge);
                    }

                }
            }
        }


        public void generateTriangles()
        {
            uint startEdge = leftmostEdge;
            //assert(startEdge != INVALID_EDGE);

            //memset(bfsVisited, 0, numEdges);
            bfsVisited = Enumerable.Repeat<bool>(false, (int)numEdges).ToArray();
            numTriangles = 0;

            // triangulation is a single connected component, so we can just traverse
            // the whole structure collecting triangles along the way
            int queueHead = 0, queueTail = 0;
            bfsQueue[queueTail++] = startEdge;
            bfsVisited[startEdge] = true;

            while (queueTail != queueHead)
            {
                uint curruint = bfsQueue[queueHead++];
                TriEdge currEdge = E[curruint];
                uint symuint = currEdge.symEdge;

                // Try to find triangle to the right hand of the edge.
                uint side1 = E[currEdge.prevCcwEdge].symEdge;
                uint side2 = E[symuint].nextCcwEdge;

                //assert(destPnt(side1) == currEdge.origPnt);
                //assert(E[side2].origPnt == destPnt(currEdge));

                if (E[side1].origPnt == destPnt(side2))
                {
                    // Three edges indeed form an oriented clockwise triangle.
                    // Let's add it to the list of triangles!
                    Triangle t = triangles[numTriangles++];
                    t.p1 = currEdge.origPnt;
                    t.p2 = E[side1].origPnt;
                    t.p3 = E[side2].origPnt;

                    // Add symmetric edges to the queue, they may belong to adjacent triangles.
                    uint side1Sym = E[side1].symEdge;
                    uint side2Sym = E[side2].symEdge;
                    if (!bfsVisited[side1Sym])
                    {
                        bfsQueue[queueTail++] = side1Sym;
                        bfsVisited[side1Sym] = true;
                    }
                    if (!bfsVisited[side2Sym])
                    {
                        bfsQueue[queueTail++] = side2Sym;
                        bfsVisited[side2Sym] = true;
                    }

                    // (Won't work as expected if triangulation has only one triangle. In this case
                    // we will add it two times: CW and CCW. But it's not worth it to add additional checks for this
                    // rare occasion).
                }

                // Add symmetric edge to the queue anyway, even if current edge does not belong to any triangle.
                if (!bfsVisited[symuint])
                {
                    bfsQueue[queueTail++] = symuint;
                    bfsVisited[symuint] = true;
                }
            }
        }
    }
}