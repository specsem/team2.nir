﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulator.DivAndConq
{
    public class TriEdge
    {
        public uint origPnt;
        public uint symEdge;
        public uint nextCcwEdge;
        public uint prevCcwEdge;
    }
}
