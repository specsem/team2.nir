using Core;
using IOSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class ParcerTest
    {
        [TestMethod]
        public void Parce()
        {
            //arrange
            var pathObj = @"..\\..\\..\\..\\..\\tests\\simple tests\\test_3x3\\test.obj";
            var pathPng = @"..\\..\\..\\..\\..\\tests\\simple tests\\test_3x3\\test.png";
            var stReader = new StreamReader(pathObj);
            var vCount = "10000";
            var expected = new List<Vertex>()
            {
                new Vertex((float)-37.5,(float)-37.5, 0),
                new Vertex((float)-37.5,(float)-37.5, 1),
                new Vertex((float)-37.5,(float)37.5, 0),
                new Vertex((float)-37.5,(float)37.5, 1),
                new Vertex(-35,-35, 1),
                new Vertex(-35,-15, 1),
                new Vertex(-35,-10, 1),
                new Vertex(-35,-10, 2),
                new Vertex(-35,10, 1),
                new Vertex(-35,10, 2),
                new Vertex(-35,15, 1),
                new Vertex(-35,15, 3),
                new Vertex(-35,35, 1),
                new Vertex(-35,35, 3),
                new Vertex(-34,-34, 2),
                new Vertex(-34,-16, 2),
                new Vertex(-34,-9, 3),
                new Vertex(-34,9, 3),
                new Vertex(-34,16, 4),
                new Vertex(-34,34, 4),
                new Vertex(-16,-34, 2),
                new Vertex(-16,-16, 2),
                new Vertex(-16,-9, 3),
                new Vertex(-16,9, 3),
                new Vertex(-16,16, 4),
                new Vertex(-16,34, 4),
                new Vertex(-15,-35, 1),
                new Vertex(-15,-15, 1),
                new Vertex(-15,-10, 1),
                new Vertex(-15,-10, 2),
                new Vertex(-15,10, 1),
                new Vertex(-15,10, 2),
                new Vertex(-15,15, 1),
                new Vertex(-15,15, 3),
                new Vertex(-15,35, 1),
                new Vertex(-15,35, 3),
                new Vertex(-10,-35, 1),
                new Vertex(-10,-35, 6),
                new Vertex(-10,-15, 1),
                new Vertex(-10,-15, 6),
                new Vertex(-10,-10, 1),
                new Vertex(-10,-10, 5),
                new Vertex(-10,10, 1),
                new Vertex(-10,10, 5),
                new Vertex(-10,15, 1),
                new Vertex(-10,15, 4),
                new Vertex(-10,35, 1),
                new Vertex(-10,35, 4),
                new Vertex(-9,-34, 7),
                new Vertex(-9,-16, 7),
                new Vertex(-9,-9, 6),
                new Vertex(-9,9, 6),
                new Vertex(-9,16, 5),
                new Vertex(-9,34, 5),
                new Vertex(9,-34, 7),
                new Vertex(9,-16, 7),
                new Vertex(9,-9, 6),
                new Vertex(9,9, 6),
                new Vertex(9,16, 5),
                new Vertex(9,34, 5),
                new Vertex(10,-35, 1),
                new Vertex(10,-35, 6),
                new Vertex(10,-15, 1),
                new Vertex(10,-15, 6),
                new Vertex(10,-10, 1),
                new Vertex(10,-10, 5),
                new Vertex(10,10, 1),
                new Vertex(10,10, 5),
                new Vertex(10,15, 1),
                new Vertex(10,15, 4),
                new Vertex(10,35, 1),
                new Vertex(10,35, 4),
                new Vertex(15,-35, 1),
                new Vertex(15,-35, 7),
                new Vertex(15,-15, 1),
                new Vertex(15,-15, 7),
                new Vertex(15,-10, 1),
                new Vertex(15,-10, 8),
                new Vertex(15,10, 1),
                new Vertex(15,10, 8),
                new Vertex(15,15, 1),
                new Vertex(15,15, 9),
                new Vertex(15,35, 1),
                new Vertex(15,35, 9),
                new Vertex(16,-34, 8),
                new Vertex(16,-16, 8),
                new Vertex(16,-9, 9),
                new Vertex(16,9, 9),
                new Vertex(16,16, 10),
                new Vertex(16,34, 10),
                new Vertex(34,-34, 8),
                new Vertex(34,-16, 8),
                new Vertex(34,-9, 9),
                new Vertex(34,9, 9),
                new Vertex(34,16, 10),
                new Vertex(34,34, 10),
                new Vertex(35,-35, 1),
                new Vertex(35,-35, 7),
                new Vertex(35,-15, 1),
                new Vertex(35,-15, 7),
                new Vertex(35,-10, 1),
                new Vertex(35,-10, 8),
                new Vertex(35,10, 1),
                new Vertex(35,10, 8),
                new Vertex(35,15, 1),
                new Vertex(35,15, 9),
                new Vertex(35,35, 1),
                new Vertex(35,35, 9),
                new Vertex((float)37.5,(float)-37.5, 0),
                new Vertex((float)37.5,(float)-37.5, 1),
                new Vertex((float)37.5,(float)37.5, 0),
                new Vertex((float)37.5,(float)37.5, 1)
            };

            //act
            var parcer = new Parcer();
            var vList = parcer.Parce(stReader, pathObj, pathPng, vCount).vList;

            //assert
            foreach(var vExpected in expected)
            {
                if (!vList.Any(v => v.x == vExpected.x && v.y == vExpected.y && v.z == vExpected.z))
                    Assert.Fail();
            }
        }
    }
}
