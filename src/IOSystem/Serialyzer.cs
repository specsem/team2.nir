﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.IO;
using System.Globalization;

namespace IOSystem
{
    public class Serialyzer
    {
        public Serialyzer()
        {
        }

        public void SerialyzeObj(Model m, StreamWriter writer)
        {
            writer.WriteLine("mtllib " + m.Name + ".mtl");

            // Если мы не будем делать экстараполяцию, то можно использовать точки, полученные при загрузки модели
            //// calculating max top, bottom, left, right values
            //float top = m.vList[0].y, bottom = m.vList[0].y, left = m.vList[0].x, right = m.vList[0].x;
            //for (int i = 0; i < m.vList.Count; i++)
            //{
            //    if (m.vList[i].y > top)
            //        top = m.vList[i].y;
            //    else if (m.vList[i].y < bottom)
            //        bottom = m.vList[i].y;
            //    if (m.vList[i].x > right)
            //        right = m.vList[i].x;
            //    else if (m.vList[i].x < left)
            //        left = m.vList[i].x;
            //}
            for (int i = 0; i < m.vList.Count; i++)
            {
                writer.WriteLine("v {0} {1} {2}",
                    m.vList[i].x.ToString(CultureInfo.InvariantCulture),
                    m.vList[i].y.ToString(CultureInfo.InvariantCulture),
                    m.vList[i].z.ToString(CultureInfo.InvariantCulture));
            }
            for (int i = 0; i < m.vList.Count; i++)
            {
                float h = (m.vList[i].x - m.left) / (m.right - m.left);
                float w = (m.vList[i].y - m.bottom) / (m.top - m.bottom);
                writer.WriteLine("vt {0} {1}",
                     h.ToString(CultureInfo.InvariantCulture),
                     w.ToString(CultureInfo.InvariantCulture));
            }
            writer.WriteLine("usemtl mat0");
            for (int i = 0; i < m.tList.Count; i++)
            {
                writer.WriteLine("f {0}/{0} {1}/{1} {2}/{2}",
                    m.tList[i].a + 1,
                    m.tList[i].b + 1,
                    m.tList[i].c + 1);
            }
        }

        public void SerialyzeMtl(Model m, StreamWriter writer)
        {
            writer.WriteLine("newmtl mat0");
            writer.WriteLine("map_Kd " + m.Name + ".png");
        }
    }
}
