﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Globalization;
using System.IO;

namespace IOSystem
{
    public class Parcer
    {
        public Parcer()
        {
        }

        public Model Parce(StreamReader reader, string objPath, string pngPath, string s_out_v_count)
        {
            //Model m = new Model(objPath, pngPath, Int32.Parse(s_out_v_count));            
            bool count_given = Int32.TryParse(s_out_v_count, out int Count);
            Model m;
            if (count_given) m = new Model(objPath, pngPath, Count);
            else m = new Model(objPath, pngPath, 0);

            string data = reader.ReadToEnd();
            string[] a_strs = data.Split('\n');

            foreach (string s in a_strs)
            {
                if (s.StartsWith("#"))
                    continue;
                if (s.StartsWith("v "))
                {
                    //thats a dot
                    string[] a_coords = s.Split();
                    float d1, d2, d3;
                    if (s.Contains(","))
                    {
                        d1 = (float)Double.Parse(a_coords[1]);
                        d2 = (float)Double.Parse(a_coords[2]);
                        d3 = (float)Double.Parse(a_coords[3]);
                    }
                    else
                    {
                        d1 = (float)Double.Parse(a_coords[1], CultureInfo.InvariantCulture);
                        d2 = (float)Double.Parse(a_coords[2], CultureInfo.InvariantCulture);
                        d3 = (float)Double.Parse(a_coords[3], CultureInfo.InvariantCulture);
                    }
                    m.vList.Add(new Vertex(d1, d2, d3));
                }
                else
                {
                    if (s.Trim().Length != 0)
                        Console.WriteLine("Unknown string in obj: {0}", s);
                }
            }

            // calculating max top, bottom, left, right values
            m.top = m.vList[0].y; m.bottom = m.vList[0].y; m.left = m.vList[0].x; m.right = m.vList[0].x;
            for (int i = 0; i < m.vList.Count; i++)
            {
                if (m.vList[i].y > m.top)
                    m.top = m.vList[i].y;
                else if (m.vList[i].y < m.bottom)
                    m.bottom = m.vList[i].y;
                if (m.vList[i].x > m.right)
                    m.right = m.vList[i].x;
                else if (m.vList[i].x < m.left)
                    m.left = m.vList[i].x;
            }

            //check that all point have diffrent x&y coords
            //TODO! rewrite it for better performance. This alg consumes n^2.
            /*for (int i = m.vList.Count-1; i > 0 ; i--)
            {
                for (int j = i - 1; j >= 0; j--)
                {
                    if ((m.vList[i].x == m.vList[j].x) && (m.vList[i].y == m.vList[j].y))
                    {
                        Console.WriteLine("dots " + i + " and " + j + " have same x and y coords.");
                        m.vList.RemoveAt(j);
                        break;
                        //Environment.Exit(1);
                    }
                }
            }*/


            if (!count_given) m.ont_v_count = m.vList.Count;
            Console.WriteLine("Parse complete. Verteces: {0}", m.vList.Count);
            return m;
        }
    }
}
