﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInfrastructure
{
    public class ResultObj
    {
        public string nameTest;
        public string nameStrategy;
        public float crit;

        public ResultObj(string nameTest, string nameStrategy, float crit)
        {
            this.nameTest = nameTest;
            this.nameStrategy = nameStrategy;
            this.crit = crit;
        }
    }
}
