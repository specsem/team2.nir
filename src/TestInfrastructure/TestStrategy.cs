﻿using Core;
using IOSystem;
using PostProcessing;
using PreProcessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Triangulator;
using SmoothInterpolator;

namespace TestInfrastructure
{
    public class TestStrategy
    {
        private static bool stopThread = false;
        public static List<ResultObj> GetResultsStrategy(string pathTestsDirectory , string pointsCount, int type, string testCount = null)
        {
            var result = new List<ResultObj>();
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(pathTestsDirectory));
            int nTestCount = dirs.Count;
            if(testCount != null)
                nTestCount = Int32.Parse(testCount);

            Parcer parcer = new Parcer();
            
            var ts = new CancellationTokenSource();
            CancellationToken ct = ts.Token;
            Task.Factory.StartNew(() => Stop(), ct);
            Console.WriteLine("Press Escape to exit");

            for (int count = 0; count < nTestCount; count++){
                string objPath = dirs[count] + "\\test.obj";
                string pngPath = dirs[count] + "\\test.png";
                string datPath = dirs[count] + "\\test.dat";
                Model model = parcer.Parce(new StreamReader(objPath), objPath, pngPath, pointsCount);
                Model modelEtalon = parcer.Parce(new StreamReader(datPath), datPath, pngPath, pointsCount);
                Triangulator.DivAndConq.DACTruangulator BWT = new Triangulator.DivAndConq.DACTruangulator();
                //Triangulator.BowyerWatson.BWTriangulator BWT = new Triangulator.BowyerWatson.BWTriangulator();
                BWT.Execute(model);

                IInterpolator Interpolator;
                String Name;
                
                switch (type)
                {
                    case 0:
                        Interpolator = new LinearInterpolator(model);
                        Name = "LinearInterpolator";
                        break;
                    case 1:
                        Interpolator = new ByClosestInterpolator(model);
                        Name = "ByClosestInterpolator";
                        break;
                    default:
                        Interpolator = new SmoothInterpolator.SmoothInterpolator(model);
                        Name = "SmoothInterpolator";
                        break;
                }

                var criterion = Criterion.Calculate(Interpolator, modelEtalon.vList);

                result.Add(new ResultObj(dirs[count].Split('\\').Last(), Name, criterion));
                Console.WriteLine("Test {0} complete", dirs[count].Split('\\').Last());
                Edge.ClearDict();
                if (stopThread)
                    break;
            }

            ts.Cancel();

            return result;
        }

        public static void Stop()
        {
            var key = Console.ReadKey();
            if(key.Key == ConsoleKey.Escape)
            {
                stopThread = true;
                Console.WriteLine("Procedure will be stopped");
            }
        }


    }
}
