﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
//using Excel = Microsoft.Office.Interop.Excel;

namespace TestInfrastructure
{
    public class Writer :IWriter
    {
        public static void WriteToCsv(string filePath, string pNum,List<ResultObj> res)
        {
            var d = Directory.GetCurrentDirectory();
            string resultFilePath =  filePath + "\\"+pNum+"-result.csv"; //имя Excel файла  

            var csv = new StringBuilder();
            if (!File.Exists(resultFilePath))
            {
                getNewCsv(csv, res, resultFilePath);
            }
            else
            {
                bool useExistingFile = false;
                var existingTestList = new List<List<string>>();
                using (var reader = new StreamReader(resultFilePath))
                {
                    String line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        existingTestList.Add(line.Split(';').ToList());
                    }

                    for(int i = 1; i < existingTestList.Count - 1; i++)
                    {
                        if(!res.Any(it => it.nameTest == existingTestList[i][0]))
                        {
                            useExistingFile = true;
                            break;
                        }
                    }

                    if (existingTestList.Count == 0 || existingTestList.Count - 2 != res.Count)
                        useExistingFile = true;
                }

                if (useExistingFile)
                {
                    getNewCsv(csv, res, resultFilePath);
                }
                else
                {
                    using(var stream = new StreamWriter(resultFilePath, false))
                    {
                        for(int i = 0; i < existingTestList.Count; i++)
                        {
                            if(i == 0)
                            {
                                existingTestList[i].Add(string.Format("{0} Criterion by {1}", res[0].nameStrategy, DateTime.Now));
                            }
                            else if(i == existingTestList.Count - 1)
                            {
                                existingTestList[i].Add(getAverage(res).ToString());
                            }
                            else
                            {
                                existingTestList[i].Add(res[i - 1].crit.ToString());
                            }
                            stream.WriteLine(WriteLine(existingTestList[i]));
                        }
                    }
                }
            }
        }

        private static string WriteLine(List<string> list)
        {
            return string.Join(";", list);
        }
        
        private static void getNewCsv(StringBuilder csv, List<ResultObj> res, string resultFilePath)
        {
            csv.AppendLine(String.Format("{0};{1} {2} {3}", "TestName", res[0].nameStrategy, "Criterion by ", DateTime.Now));

            foreach (var obj in res)
            {
                csv.AppendLine(String.Format("{0};{1}", obj.nameTest, obj.crit.ToString()));
            }

            csv.AppendLine(string.Format("Average;{0}", getAverage(res)));

            File.WriteAllText(resultFilePath, csv.ToString());
        }

        private static float getAverage(List<ResultObj> list)
        {
            return list.Sum(it => it.crit) / list.Count;
        }
    }

}
