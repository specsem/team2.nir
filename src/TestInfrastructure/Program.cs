﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInfrastructure
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Two args required. [path_to_folder] [num of verteces] [num of tests]");
                return;
            }
            List<ResultObj> result;
            result = TestStrategy.GetResultsStrategy(args[0], args[1], 0, args.Length == 3 ? args[2] : null);
            Writer.WriteToCsv(args[0], args[1], result);
            result = TestStrategy.GetResultsStrategy(args[0], args[1], 1, args.Length == 3 ? args[2] : null);
            Writer.WriteToCsv(args[0], args[1], result);
            result = TestStrategy.GetResultsStrategy(args[0], args[1], 2, args.Length == 3 ? args[2] : null);
            Writer.WriteToCsv(args[0], args[1], result);
        }
    }
}
