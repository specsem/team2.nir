﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace PostProcessing
{
    public class MakeClockwise : IStrategy
    {
        public void Execute(Model m)
        {
            foreach (Triad t in m.tList)
            {
                t.MakeClockwise(m.vList);
            }
        }
    }
}
