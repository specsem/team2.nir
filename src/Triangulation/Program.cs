using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Triangulation;
using System.Diagnostics;
using SmoothInterpolator;

using PreProcessing;

namespace Triangulation
{
    class Program
    {
        static void Main(string[] args)
        {
            //Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));
            if ((args.Length < 2)||(args.Length > 3))
            {
                Console.WriteLine("Three or two args required. [path_to_obj] [path_to_texture] [num of verteces](optional)");
                Console.ReadLine();
                return;
            }

            if (args.Length == 3)
            {
                if (Int32.TryParse(args[2], out int Count))
                {
                    if ((Count < 13) || (Count > 8500000))
                    {
                        Console.WriteLine("Incorrect number of vertices");
                        Console.ReadLine();
                        return;
                    }
                }
            }

            Stopwatch watch = new Stopwatch();

            watch.Start();

            string str;
            if (args.Length == 3) str = args[2];
            else str = "Nope";
            IOSystem.Parcer p = new IOSystem.Parcer();
            Model m = p.Parce(new StreamReader(args[0]), args[0], args[1], str);

            watch.Stop();
            Console.WriteLine("Parsing:" + watch.ElapsedMilliseconds + " ms");

            

            watch.Restart();

            //Triangulator.BowyerWatson.BWTriangulator BWT = new Triangulator.BowyerWatson.BWTriangulator();
            Triangulator.DivAndConq.DACTruangulator BWT = new Triangulator.DivAndConq.DACTruangulator();
            BWT.Execute(m);

            watch.Stop();
            Console.WriteLine("Triangulation:" + watch.ElapsedMilliseconds + " ms");



            watch.Restart();

            SmoothInterpolator.SmoothInterpolator SI = new SmoothInterpolator.SmoothInterpolator(m);            
            m.PrepareForDestruction();

            watch.Stop();
            Console.WriteLine("Interpolator construction:" + watch.ElapsedMilliseconds + " ms");



            watch.Restart();

            StreamWriter sobj = new StreamWriter(Path.GetDirectoryName(m.objFilePath) + "\\" + m.Name + "_triang.obj");
            ObjModel om = new ObjModel(m, sobj);
            EquilateralFacetApproximation A = new EquilateralFacetApproximation(SI);
            A.Execute(om);

            watch.Stop();
            Console.WriteLine("Approximation construction:" + watch.ElapsedMilliseconds + " ms");




            watch.Restart();

            IOSystem.Serialyzer s = new IOSystem.Serialyzer();

            StreamWriter smtl = new StreamWriter(Path.GetDirectoryName(m.objFilePath) + "\\" + m.Name + ".mtl");
            s.SerialyzeMtl(m, smtl);
            smtl.Close();

            watch.Stop();
            Console.WriteLine("Output files generation:" + watch.ElapsedMilliseconds + " ms");            

            Console.ReadLine();
        }
    }
}


/*

    Pipeline pipe = new Pipeline();
            // PreProcessing
            //pipe.Add(new PreProcessing.InterpolationProvider(new PreProcessing.GridScatterStrategy(), new PreProcessing.ByNClosestInterpolator(m,3,2)));
            // Triangulation
            //pipe.Add(new Triangulator.BowyerWatson.BWTriangulator());
            // PostProcessing
            //pipe.Add(new PostProcessing.MakeClockwise());
 
*/


//pipe.Add(new PreProcessing.EquilateralFacetApproximation(SI));

//pipe.Add(new PreProcessing.EquilateralFacetApproximation(new PreProcessing.ByNClosestInterpolator(m,3,2)));

    /*
Stopwatch watch = new Stopwatch();
watch.Start();
            //pipe.Execute(m);
            watch.Stop();

    */