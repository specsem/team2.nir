﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace PreProcessing
{
    public class FullWeightedInterpolator : IInterpolator
    {
        Model m;
        float specialCase = (float)(1.175494351 * (1 / Math.Pow((double)10, (double)36)));
        public FullWeightedInterpolator(Model m)
        {
            this.m = m;
        }
        public float F(float x, float y)
        {
            float result, weighted_sum = 0, total_weight = 0;
            Vertex v = new Vertex(x, y);
            for (int i = 0; i < m.vList.Count; i++)
            {
                float dist = v.D2_distance2To(m.vList[i]);
                if (dist < specialCase)
                {
                    return m.vList[i].z;
                }
                weighted_sum += m.vList[i].z / dist;
                total_weight += 1 / dist;
            }
            result = weighted_sum / total_weight;
            return result;
        }
    }
}
