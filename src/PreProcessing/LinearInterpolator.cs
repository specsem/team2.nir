﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Logger;

namespace PreProcessing
{
    public class LinearInterpolator : IInterpolator
    {
        Model m;
        int log_writes = 0;
        public LinearInterpolator(Model _m)
        {
            m = _m;
            /*m = _m.Clone() as Model;
            IStrategy tr = new Triangulator.BowyerWatson.BWTriangulator();
            tr.Execute(m);*/
        }

        public float F(float x, float y)
        {
            Vertex v1 = new Vertex(x, y);
            // v already in the heap
            foreach (Vertex v in m.vList)
            {
                if ((v1.x == v.x) && (v1.y == v.y))
                    return v.z;
            }
            foreach (Triad t1 in m.tList)
            {

                if (PointInTriangle(v1, t1))
                {
                    // find point of intersection of a-v1 and b-c
                    Vertex a = m.vList[t1.a];
                    Vertex o1 = FindIntersection(v1, t1);

                    float ret = 0;
                    ret = (a.z - o1.z);
                    ret /= a.D2_distanceTo(o1);
                    ret *= a.D2_distanceTo(v1);
                    ret = a.z - ret;
                    return ret;
                }
                else if (PointOnEdge(v1, m.vList[t1.a], m.vList[t1.b]) > 0)
                {
                    return PointOnEdgeFindZ(v1, m.vList[t1.a], m.vList[t1.b]);
                }
                else if (PointOnEdge(v1, m.vList[t1.b], m.vList[t1.c]) > 0)
                {
                    return PointOnEdgeFindZ(v1, m.vList[t1.b], m.vList[t1.c]);
                }
                else if (PointOnEdge(v1, m.vList[t1.c], m.vList[t1.a]) > 0)
                {
                    return PointOnEdgeFindZ(v1, m.vList[t1.c], m.vList[t1.a]);
                }
            }
            // BUG! Write down the case, when vertex is on the line of triad.
            if (log_writes < 5)
            {
                Log.Error("BUG. Projected point is out of boundaries.");
                log_writes++;
            }
            return 0.0f;
        }
        
        // TODO move those methods to Vertex class

        Vertex FindIntersection(Vertex v1, Triad t1)
        {
            return (FindIntersection(v1, m.vList[t1.a], m.vList[t1.b], m.vList[t1.c]));
        }

        Vertex FindIntersection(Vertex v, Vertex a, Vertex b, Vertex c)
        {
            Vertex o1 = new Vertex(((v.x * a.y - v.y * a.x) * (b.x - c.x) - (v.x - a.x) * (b.x * c.y - b.y * c.x)) / ((v.x - a.x) * (b.y - c.y) - (v.y - a.y) * (b.x - c.x)),
                ((v.x * a.y - v.y * a.x) * (b.y - c.y) - (v.y - a.y) * (b.x * c.y - b.y * c.x)) / ((v.x - a.x) * (b.y - c.y) - (v.y - a.y) * (b.x - c.x))
                );

            o1.z = (b.z - c.z);
            o1.z /= b.D2_distanceTo(c);
            o1.z *= b.D2_distanceTo(o1);
            o1.z = b.z - o1.z;
            return o1;
        }

        float sign(Vertex p1, Vertex p2, Vertex p3)
        {
            return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
        }

        bool PointInTriangle(Vertex pt, Vertex v1, Vertex v2, Vertex v3)
        {
            bool b1, b2, b3;

            b1 = sign(pt, v1, v2) < 0.0f;
            b2 = sign(pt, v2, v3) < 0.0f;
            b3 = sign(pt, v3, v1) < 0.0f;

            return ((b1 == b2) && (b2 == b3));
        }

        bool PointInTriangle(Vertex pt, Triad tr)
        {
            return PointInTriangle(pt, m.vList[tr.a], m.vList[tr.b], m.vList[tr.c]);
        }

        float PointOnEdge(Vertex v1, Vertex a, Vertex b)
        {
            float x = b.x - a.x;
            float y = b.y - a.y;
            float q = (v1.x - a.x) / x;
            float w = (v1.y - a.y) / y;
            if ((x == 0) && (v1.x == a.x))
            {
                if ((w >= 0) && (w <= 1))
                    return w;
            }
            if ((y == 0) && (v1.y == a.y))
            {
                if ((q >= 0) && (q <= 1))
                    return q;
            }
            if ((q == w) && (q>=0) && (q<=1))
                return q;
            return -1;
        }

        float PointOnEdgeFindZ(Vertex v1, Vertex a, Vertex b)
        {
            float d1 = a.D2_distanceTo(b);
            float d2 = a.D2_distanceTo(v1);
            float z = b.z - a.z;
            return a.z + (z / d1 * d2);
        }
    }
}
