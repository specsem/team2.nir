﻿using System;
using Core;
using System.Collections.Generic;
using System.IO;
namespace PreProcessing
{
    public class ByNClosestInterpolator : IInterpolator
    {
        Model m;
        int numberOfPoints;
        float pow;
        float specialCase =(float)(1.175494351 *(1 / Math.Pow((double)10, (double)36)));
        public ByNClosestInterpolator(Model m, int numberOfPoints, float pow) // for best results, numberOfPoints = 3 or 4; pow = 2
        {
            this.m = m;
            this.numberOfPoints = numberOfPoints;
            this.pow = pow;
        }
        public float F(float x,float y)
        {
            float result, weighted_sum=0,total_weight=0;
            Vertex v = new Vertex(x, y);
            List<Vertex> nearestPoints = FindNearestPoints(v);
            List<float> distances = FindDistanceToPoints(nearestPoints, v);
            for (int i = 0; i < numberOfPoints;i++)
            {
                if(distances[i]<specialCase){
                    return nearestPoints[i].z;
                }
                weighted_sum += nearestPoints[i].z / distances[i];
                total_weight += 1 / distances[i];
            }
            result = weighted_sum / total_weight;
            return result;
        }
        private List<Vertex> FindNearestPoints (Vertex vertex)
        {
            List<Vertex> result = new List<Vertex>();
            List<Vertex> localPoints = new List<Vertex>(m.vList);
            for (int i = 0; i < numberOfPoints;i++)
            {
                int positionOfNearestPoint = FindPositionOfNearestPoint(localPoints, vertex);
                result.Add(localPoints[positionOfNearestPoint]);
                localPoints.RemoveAt(positionOfNearestPoint);
            }
            return result;            
        }
        private int FindPositionOfNearestPoint(List<Vertex> points,Vertex vertex)
        {
            int positionOfNearestPoint = 0;
            float min = points[0].D2_distance2To(vertex);
            for (int i = 1; i < points.Count;i++)
            {
                if(min>points[i].D2_distance2To(vertex))
                {
                    min = points[i].D2_distance2To(vertex);
                    positionOfNearestPoint = i;
                }                
            }
            return positionOfNearestPoint;
        }
        private List<float> FindDistanceToPoints(List<Vertex> points,Vertex from)
        {
            List<float> result = new List<float>();
            for (int i = 0; i < points.Count;i++)
            {
                result.Add( (float) Math.Pow(from.D2_distanceTo(points[i]),pow) );
            }
            return result;
        }
    }
}
