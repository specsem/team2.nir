﻿using Core;
using System;
using System.Collections.Generic;

namespace PreProcessing
{
    public static class Criterion
    {
        public static float Calculate(IInterpolator func, List<Vertex> vList)
        {
            float result = 0;
            float zmax = vList[0].z;
            float zmin = vList[0].z;
            
            foreach(var vertex in vList)
            {
                if (vertex.z > zmax) zmax = vertex.z;
                if (vertex.z < zmin) zmin = vertex.z;
                result += Math.Abs(func.F(vertex.x, vertex.y) - vertex.z);
            }
            return (result / vList.Count) / (zmax - zmin);
        }
    }
}
