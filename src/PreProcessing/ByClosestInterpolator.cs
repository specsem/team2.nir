﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace PreProcessing
{
    public class ByClosestInterpolator : IInterpolator
    {
        Model m;
        public ByClosestInterpolator(Model _m)
        {
            m = _m;
        }

        public float F(float x, float y)
        {
            Vertex v = new Vertex(x, y);
            Vertex nearest = m.vList[0];
            float closest = nearest.D2_distance2To(v);
            // находим ближайшую точку
            foreach(Vertex vi in m.vList)
            {
                float f = vi.D2_distance2To(v);
                if (f<closest)
                {
                    closest = f;
                    nearest = vi;
                }
            }
            return nearest.z;
        }
    }
}
