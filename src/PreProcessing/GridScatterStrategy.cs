﻿using Core;
using System;
using System.Collections.Generic;

namespace PreProcessing
{
    public class GridScatterStrategy : IScatterStrategy
    {
        public List<Vertex> GetScatterVertex(IInterpolator interpolator, Model model, int vCount, float xMin, float yMin, float xMax, float yMax)
        {
            var vList = new List<Vertex>();
            var sRect = (xMax - xMin) * (yMax - yMin);
            Random random = new Random();

            //float zzz = interpolator.F(1.3f, 1.3f);

            vList.Add(new Vertex(xMin, yMin, interpolator.F(xMin, yMin)));
            vList.Add(new Vertex(xMin, yMax, interpolator.F(xMin, yMax)));
            vList.Add(new Vertex(xMax, yMin, interpolator.F(xMax, yMin)));
            vList.Add(new Vertex(xMax, yMax, interpolator.F(xMax, yMax)));

            //square for 1 vertex
            vCount -= 4;
            var sVertex = sRect / vCount;

            //length of a side of a square
            var side = (float) Math.Sqrt(sVertex);

            //for(float xCur = xMin; xCur < xMax; xCur += side)
            //{
            //    vList.Add(new Vertex(xCur, yMin, interpolator.F(xCur, yMin)));
            //    vList.Add(new Vertex(xCur, yMax, interpolator.F(xCur, yMax)));
            //}

            //for (float yCur = yMin + side; yCur < yMax; yCur += side)
            //{
            //    vList.Add(new Vertex(xMin, yCur, interpolator.F(xMin, yCur)));
            //    vList.Add(new Vertex(xMax, yCur, interpolator.F(xMax, yCur)));
            //}

            for (float xCur = xMin + side; xCur < xMax; xCur += side)
            {
                for (float yCur = yMin + side; yCur < yMax; yCur += side)
                {
                    if (vList.Count < vCount)
                    {
                        vList.Add(new Vertex(xCur, yCur, interpolator.F(xCur, yCur)));
                    }
                }
            }

            //while(vList.Count < vCount)
            //{
            //    float newX = (float) random.NextDouble() * (xMax - xMin) + xMin;
            //    float newY = (float) random.NextDouble() * (yMax - yMin) + yMin;
            //    vList.Add(new Vertex(newX, newY, interpolator.F(newX, newY)));
            //}

            return vList;
        }
    }
}
