﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Globalization;

namespace PreProcessing
{
    public class EquilateralFacetApproximation
    { 
        private IInterpolator interpolator;
        private int ToAdd;
        //private List<Vertex> vList;
        //private List<Triad> tList;
        private ObjModel m;
        int vi, ti;
        int[] A, B, C;
        float[] X, Y, Z;

        public EquilateralFacetApproximation(IInterpolator interpolator) // вся аппроксимация строится по интерполятору
        {
            this.interpolator = interpolator;
        }       

        public void Execute(ObjModel m) // модель нужна только для границ аппроксимации, кол-ва точек и текстуры
        {
            this.m = m; // ВАЖНО: сделать что-то, чтобы не было конфликта интерполятора и модели
            Coefficients(out float xmin, out float ymin, out int N, out int x, out int y, out float u, out float h);
            X = new float[N];
            Y = new float[N];
            Z = new float[N];
            this.A = new int[2* N];
            B = new int[2*N];
            C = new int[2*N];
            vi = 0;
            ti = 0;
            ToAdd = N - n(x, y);
            for (int i = 0; i <= x; i++) // внешняя сетка
            {
                for (int j = 0; j <= y; j++)
                {
                    X[vi] = xmin + i * u;
                    Y[vi] = ymin + j * h;
                    Z[vi] = interpolator.F(X[vi], Y[vi]);
                    vi++;
                }
            }
            float newxmin = xmin + u / 2;
            float newymin = ymin + h / 2;
            for (int i = 0; i < x; i++) // внутренняя сетка
            {
                for (int j = 0; j < y; j++)
                {
                    X[vi] = newxmin + i * u;
                    Y[vi] = newymin + j * h;
                    Z[vi] = interpolator.F(X[vi], Y[vi]);
                    vi++;
                }
            }            
            int A = (x + 1) * (y + 1); // точек во внешней сетке
            int L = A;
            for (int j = 0; j < y; j++) //первый столбец треугольников
            {
                int G = j;
                int H = G + y + 1;
                int K = L + y;
                CheckTriad(L, G, H);
                CheckTriad(L, H, K);
                CheckTriad(L, K, H + 1);
                CheckTriad(L, H + 1, G + 1);
                L++;
            }
            for (int i = 1; i < x - 1; i++) // треугольники
            {
                for (int j = 0; j < y; j++)
                {
                    int G = i * (y + 1) + j;
                    int H = G + y + 1;
                    int K = L + y;
                    AddTriad(L, G, H);
                    AddTriad(L, H, K);
                    AddTriad(L, K, H + 1);
                    AddTriad(L, H + 1, G + 1);
                    L++;
                }
            }
            for (int j = 0; j < y; j++) // последний столбец треугольников
            {
                int G = (x - 1) * (y + 1) + j;
                int H = x * (y + 1) + j;
                AddTriad(L, G, H);
                AddTriad(L, H + 1, G + 1);
                AddTriad(L, H, H + 1);
                AddTriad(A + j, j + 1, j);
                L++;
            }

            Serialyze();
            
            if (N != vi)
            {
                Console.WriteLine("ERROR: EquilateralFacetApproximation");
                Console.WriteLine("Number of vertices generated is not equal to the number of vertices required");
                Console.WriteLine("Please contact Ivan Shumilov");
                Console.ReadLine();
            }
        }

        private void AddTriad(int a, int b, int c)
        {
            A[ti] = a;
            B[ti] = b;
            C[ti] = c;
            ti++;
        }

        private void CheckTriad(int a, int b, int c)// для добавления недостающих до N точек
        {
            if (ToAdd == 0) AddTriad(a, b, c);
            else
            {
                X[vi] = (X[a] + X[b] + X[c]) / 3;
                Y[vi] = (Y[a] + Y[b] + Y[c]) / 3;
                Z[vi] = interpolator.F(X[vi], Y[vi]);                
                ToAdd--;
                AddTriad(vi, a, b);
                AddTriad(vi, b, c);
                AddTriad(vi, c, a);
                vi++;
            }
        }

        private void Coefficients(out float xmin, out float ymin, out int N, out int x, out int y, out float u, out float h) // Чёрная магия
        {
            float xmax = m.right;
            xmin = m.left;
            float ymax = m.top;
            ymin = m.bottom;
            float a = xmax - xmin;
            float b = ymax - ymin;
            N = m.ont_v_count;           
            float k = 0.28867513459f * b / a;
            float p = 0.25f + 0.5f * k;
            y = (int)Math.Round(Math.Sqrt(p * p + k * (N - 1)) - p);
            x = (int)Math.Round((float)(N - y - 1) / (2 * y + 1));
            while (true)
            {
                int n0 = n(x, y);
                bool xm = (x < y);
                int min = xm ? 2 * x + 1 : 2 * y + 1;
                int dif = Math.Abs(N - n0);
                if (dif < min) break;
                int sign = Math.Sign(N - n0);
                if (xm) y += sign * (dif / min);
                else x += sign * (dif / min);
            }
            while (N < n(x, y))
            {
                if (x < y) y--;
                else x--;
            }
            u = a / x;
            h = b / y;
        }

        private static int n(int x, int y)// будет сгенерировано точек в треугольной сетке по к-там x,y
        {
            return (x + 1) * (2 * y + 1) - y;
        }

        private void Serialyze()
        {
            m.sw.WriteLine("mtllib " + m.Name + ".mtl");
            for (int i = 0; i < vi; i++)
            {
                m.sw.WriteLine("v {0} {1} {2}",
                    X[i].ToString(CultureInfo.InvariantCulture),
                    Y[i].ToString(CultureInfo.InvariantCulture),
                    Z[i].ToString(CultureInfo.InvariantCulture));
            }
            for (int i = 0; i < vi; i++)
            {
                float h = (X[i] - m.left) / (m.right - m.left);
                float w = (Y[i] - m.bottom) / (m.top - m.bottom);
                m.sw.WriteLine("vt {0} {1}",
                     h.ToString(CultureInfo.InvariantCulture),
                     w.ToString(CultureInfo.InvariantCulture));
            }
            m.sw.WriteLine("usemtl mat0");
            for (int i = 0; i < ti; i++)
            {
                m.sw.WriteLine("f {0}/{0} {1}/{1} {2}/{2}",
                    A[i] + 1,
                    B[i] + 1,
                    C[i] + 1);
            }
            m.sw.Close();
        }
    }
}
