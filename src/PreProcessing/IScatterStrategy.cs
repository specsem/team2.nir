﻿using Core;
using System.Collections.Generic;

namespace PreProcessing
{
    public interface IScatterStrategy
    {
        List<Vertex> GetScatterVertex(IInterpolator interpolator, Model model, int vCount, float xMin, float yMin, float xMax, float yMax);
    }
}
