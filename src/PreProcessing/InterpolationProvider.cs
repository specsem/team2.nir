﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace PreProcessing
{
    public class InterpolationProvider : IStrategy
    {
        IScatterStrategy scat_strategy;
        IInterpolator interpol_strategy;
        public InterpolationProvider(IScatterStrategy _scat_strategy, IInterpolator _interpol_strategy)
        {
            scat_strategy = _scat_strategy;
            interpol_strategy = _interpol_strategy;
        }
        public void Execute(Model m)
        {
            List<Vertex> lst = scat_strategy.GetScatterVertex(interpol_strategy, m, m.ont_v_count, m.left, m.bottom, m.right, m.top);
            m.vList = lst;
        }
    }
}
