﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreProcessing
{
    public interface IInterpolator
    {
        float F(float x, float y);
    }
}
