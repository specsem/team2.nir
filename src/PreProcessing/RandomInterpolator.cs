﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Logger;

namespace PreProcessing
{
    public class RandomInterpolator : IInterpolator
    {
        float zmax, zmin;
        Random r;
        public RandomInterpolator(List<Vertex> vList)
        {
            r = new Random();
            zmax = vList[0].z;
            zmin = vList[0].z;

            foreach (var vertex in vList)
            {
                if (vertex.z > zmax) zmax = vertex.z;
                if (vertex.z < zmin) zmin = vertex.z;
            }
        }

        public float F(float x, float y)
        {
            return zmin + (float)(r.NextDouble() * (zmax - zmin));
        }
    }
}
