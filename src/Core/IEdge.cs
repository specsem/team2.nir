﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IEdge
    {
        List<Triad> Belongs { get; set; }
        Vertex A { get; }
        Vertex B { get; }
        int MyCompareTo(object obj);
        bool IsContainPoint(Vertex p);
        bool IsHavingPoint(Vertex p);
    }
}
