﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Edge : IEdge
    {
        static private Dictionary<Tuple<Vertex, Vertex>, IEdge> dict = new Dictionary<Tuple<Vertex, Vertex>, IEdge>();
        private List<Triad> belongs = new List<Triad>(2);
        private Vertex a, b;
        //public bool orient;

        public List<Triad> Belongs { get => belongs; set => belongs = value; }
        public Vertex A { get => a; }
        public Vertex B { get => b; }

        private Edge(Vertex _a, Vertex _b)
        {
            a = _a;
            b = _b;
            //orient = true;
        }
        //public int CompareTo(object obj)
        //{
        //    Edge a = obj as Edge;
        //    if (a.a.CompareTo(this.a) < 0) return 1;
        //    if (a.a.CompareTo(this.a) > 0) return 1;
        //    if (a.b.CompareTo(this.b) < 0) return -1;
        //    if (a.b.CompareTo(this.b) > 0) return 1;
        //    return 0;
        //}
        public int MyCompareTo(object obj)
        {
            IEdge a = obj as IEdge;
            if ((a.A.CompareTo(this.a) == 0) && (a.B.CompareTo(this.b) == 0)) return 0;
            if ((a.A.CompareTo(this.b) == 0) && (a.B.CompareTo(this.a) == 0)) return 0;
            return 1;
        }
        static public IEdge GetEdge(Vertex _a, Vertex _b)
        {
            IEdge ret;
            // dangerous
            if (!(dict.TryGetValue(Tuple.Create<Vertex, Vertex>(_a, _b), out ret)))
            {
                if (dict.TryGetValue(Tuple.Create<Vertex, Vertex>(_b, _a), out ret))
                {
                    ret = new ReverseEdge(ret);
                }
                else
                {
                    ret = new Edge(_a, _b);
                    dict.Add(Tuple.Create<Vertex, Vertex>(_a, _b), ret);
                }
            }
            return ret;
        }
        static void RemoveEdge(Edge e)
        {
            dict.Remove(Tuple.Create<Vertex, Vertex>(e.a, e.b));
        }

        public override string ToString()
        {
            return (this.a.ToString());
        }


        public bool IsHavingPoint(Vertex p)
        {
            float dxc = p.x - A.x;
            float dyc = p.y - A.y;

            float dxl = B.x - A.x;
            float dyl = B.y - A.y;

            float cross = dxc * dyl - dyc * dxl;
            if (cross != 0)
                return false;
            if ((p.x <= A.x && p.x >= B.x) || (p.x >= A.x && p.x <= B.x))
            {
                if ((p.y <= A.y && p.y >= B.y) || (p.y >= A.y && p.y <= B.y))
                    return true;
            }
            return false;
        }

        public bool IsContainPoint(Vertex p)
        {
            float dxc = p.x - A.x;
            float dyc = p.y - A.y;

            float dxl = B.x - A.x;
            float dyl = B.y - A.y;

            float cross = dxc * dyl - dyc * dxl;
            if (cross != 0)
                return false;
            return true;
        }

        static public void ClearDict()
        {
            dict = new Dictionary<Tuple<Vertex, Vertex>, IEdge>();
        }
    }
}
