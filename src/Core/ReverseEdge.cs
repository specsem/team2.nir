﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ReverseEdge : IEdge
    {
        private IEdge e;
        public Vertex A { get => e.B; }
        public Vertex B { get => e.A; }
        public List<Triad> Belongs { get => e.Belongs; set => e.Belongs = value; }

        public ReverseEdge(IEdge _e)
        {
            e = _e;
        }

        public int MyCompareTo(object obj)
        {
            return e.MyCompareTo(obj);
        }
        public override string ToString()
        {
            return (this.A.ToString());
        }

        public bool IsHavingPoint(Vertex p)
        {
            return e.IsHavingPoint(p);
        }

        public bool IsContainPoint(Vertex p)
        {
            return e.IsContainPoint(p);
        }
    }
}
