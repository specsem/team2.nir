﻿using System;

namespace Core
{
    public class Vertex : IComparable
    {
        public float x, y, z;
        public int ind;

        protected Vertex() { }

        public Vertex(float x, float y)
        {
            this.x = x; this.y = y; this.z = (float)1.0;
        }

        public Vertex(float x, float y, float z)
        {
            this.x = x; this.y = y; this.z = z;
        }

        public float D2_distance2To(Vertex other)
        {
            float dx = x - other.x;
            float dy = y - other.y;
            return dx * dx + dy * dy;
        }

        public float D2_distanceTo(Vertex other)
        {
            return (float)Math.Sqrt(D2_distance2To(other));
        }

        public float DistanceTo(Vertex p)
        {
            return (float)Math.Sqrt(Math.Pow((x - p.x), 2) + Math.Pow((y - p.y), 2));
        }

        public override string ToString()
        {
            return string.Format("({0},{1}, {2})", x, y, z);
        }

        public int CompareTo(object obj)
        {
            Vertex p = obj as Vertex;

            if (y != p.y)
                return y.CompareTo(p.y);
            else
                return x.CompareTo(p.x);
        }

        public float i { get => x; set => x = value; }
        public float j { get => y; set => y = value; }

    }
}
