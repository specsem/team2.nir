﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Core
{
    public class Model : AModel, ICloneable
    {
        public List<Vertex> vList;
        public List<Triad> tList;
        /*public string Name;
        public string objFilePath;
        public string pngFilePath;
        public int ont_v_count;
        public float top, bottom, left, right;*/

        public Model(string objFP, string pngFP, int _ont_v_count)
        {   
            vList = new List<Vertex>();
            tList = new List<Triad>();
            objFilePath = objFP;
            pngFilePath = pngFP;
            Name = Path.GetFileNameWithoutExtension(objFilePath);
            ont_v_count = _ont_v_count;
        }

        public override void AddTriad(int a, int b, int c)
        {
            tList.Add(new Triad(vList[a], vList[b], vList[c]));
        }

        public override void AddVertex(float x, float y, float z)
        {
            vList.Add(new Vertex(x, y, z));
        }

        public void PrepareForDestruction()
        {
            for (int i = 0; i < tList.Count;i++)
            {
                tList[i].PrepareForDestruction();
            }
        }

        public object Clone()
        {
            Model m = new Model(objFilePath, pngFilePath, ont_v_count);
            m.vList = new List<Vertex>(this.vList);
            m.tList = new List<Triad>(this.tList);
            m.top = top;
            m.bottom = bottom;
            m.left = left;
            m.right = right;
            return m;
        }
    }
}
