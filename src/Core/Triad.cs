﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Core
{
    public class Triad
    {
        //public Triad[] neibh = new Triad[3];
        //public Vertex a, b, c;
        public IEdge ab, bc, ca;
        public Vertex circumcircle;
        public float radius;
        public int num;

        public int a { get => ab.A.ind; }
        public int b { get => bc.A.ind; }
        public int c { get => ca.A.ind; }

        public Triad(IEdge e1, Vertex p1)
        {
            ab = e1;
            bc = Edge.GetEdge(e1.B, p1);
            ca = Edge.GetEdge(p1, e1.A);

            ab.Belongs.Add(this);
            bc.Belongs.Add(this);
            ca.Belongs.Add(this);
            CalcCenter();
        }

        public Triad(Vertex _a, Vertex _b, Vertex _c)
        {
            ab = Edge.GetEdge(_a, _b);
            bc = Edge.GetEdge(_b, _c);
            ca = Edge.GetEdge(_c, _a);

            ab.Belongs.Add(this);
            bc.Belongs.Add(this);
            ca.Belongs.Add(this);
            CalcCenter();
        }

        /// <summary>
        /// If current orientation is not clockwise, swap b<->c
        /// </summary>
        public void MakeClockwise(List<Vertex> Vertexs)
        {
            //float centroidX = (Vertexs[a].x + Vertexs[b].x + Vertexs[c].x) / 3.0f;
            //float centroidY = (Vertexs[a].y + Vertexs[b].y + Vertexs[c].y) / 3.0f;

            //float dr0 = Vertexs[a].x - centroidX, dc0 = Vertexs[a].y - centroidY;
            //float dx01 = Vertexs[b].x - Vertexs[a].x, dy01 = Vertexs[b].y - Vertexs[a].y;

            //float df = -dx01 * dc0 + dy01 * dr0;
            //if (df > 0)
            //{
            //    // Need to swap vertices b<->c and edges ab<->bc
            //    int t = b;
            //    b = c;
            //    c = t;

            //    t = ab;
            //    ab = ac;
            //    ac = t;
            //}
            throw new NotImplementedException();
        }

        public bool isSame(Triad t)
        {
            //ABC-ABC
            if ((ab.A.CompareTo(t.ab.A) == 0))
            {
                if ((bc.A.CompareTo(t.bc.A) == 0) && (ca.A.CompareTo(t.ca.A) == 0))
                    return true;
                if ((bc.A.CompareTo(t.ca.A) == 0) && (ca.A.CompareTo(t.bc.A) == 0))
                    return true;
            }
            if ((ab.A.CompareTo(t.bc.A) == 0))
            {
                if ((bc.A.CompareTo(t.bc.A) == 0) && (ca.A.CompareTo(t.ab.A) == 0))
                    return true;
                if ((bc.A.CompareTo(t.ab.A) == 0) && (ca.A.CompareTo(t.bc.A) == 0))
                    return true;
            }
            if ((ab.A.CompareTo(t.ca.A) == 0))
            {
                if ((bc.A.CompareTo(t.bc.A) == 0) && (ca.A.CompareTo(t.ab.A) == 0))
                    return true;
                if ((bc.A.CompareTo(t.ab.A) == 0) && (ca.A.CompareTo(t.bc.A) == 0))
                    return true;
            }
            return false;
        }

        public void PrepareForDestruction()
        {
            ab.Belongs.Remove(this);
            bc.Belongs.Remove(this);
            ca.Belongs.Remove(this);
        }

        public bool IsContain(IEdge e)
        {
            if ((ab.MyCompareTo(e) == 0) || (bc.MyCompareTo(e) == 0) || (ca.MyCompareTo(e) == 0)) return true;
            else return false;
        }
        public bool IsContain(Vertex p)
        {
            if ((ab.A.CompareTo(p) == 0) || (bc.A.CompareTo(p) == 0) || (ca.A.CompareTo(p) == 0)) return true;
            else return false;
        }

        private void CalcCenter()
        {
            Vertex a = ab.A, b = ab.B, c = ca.A;

            float d1 = (b.x * b.x + b.y * b.y - c.x * c.x - c.y * c.y);
            float d2 = (c.x * c.x + c.y * c.y - a.x * a.x - a.y * a.y);
            float d3 = (a.x * a.x + a.y * a.y - b.x * b.x - b.y * b.y);
            float d4 = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));

            double cx = -0.5 * (a.y * d1 + b.y * d2 + c.y * d3) / d4;
            double cy = 0.5 * (a.x * d1 + b.x * d2 + c.x * d3) / d4;

            circumcircle = new Vertex((float)cx, (float)cy);
            radius = circumcircle.D2_distanceTo(a);
            if (float.IsNaN(radius) || float.IsInfinity(radius))
            {
                int aaa = 0;
            }
        }

        // true - inside
        // false - outside or on border
        public bool VertexInCircle(Vertex pt)
        {
            if (pt.DistanceTo(circumcircle) < radius)
                return true;
            return false;
        }

        public bool VertexInTriangle(Vertex pt)
        {
            return VertexInTriangle(pt, this.ab.A, this.bc.A, this.ca.A);
        }

        float sign(Vertex p1, Vertex p2, Vertex p3)
        {
            return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
        }

        bool VertexInTriangle(Vertex pt, Vertex v1, Vertex v2, Vertex v3)
        {
            bool b1, b2, b3;

            b1 = sign(pt, v1, v2) < 0.0f;
            b2 = sign(pt, v2, v3) < 0.0f;
            b3 = sign(pt, v3, v1) < 0.0f;

            return ((b1 == b2) && (b2 == b3));
        }
        //public override string ToString()
        //{
        //    return string.Format("Triad vertices {0} {1} {2} ; edges {3} {4} {5}", a, b, c, ab, ac, bc);
        //}
    }
}
