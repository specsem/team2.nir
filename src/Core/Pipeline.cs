﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Pipeline : IStrategy
    {
        public List<IStrategy> slst;

        public Pipeline()
        {
            slst = new List<IStrategy>();
        }

        public void Add(IStrategy s)
        {
            slst.Add(s);
        }

        public void Execute(Model m)
        {
            foreach (IStrategy s in slst)
            {
                s.Execute(m);
            }
        }
    }
}
