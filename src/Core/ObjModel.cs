﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace Core
{
    public class ObjModel : AModel // serializer;
    {
        /*public string Name;
        public int ont_v_count;
        public string objFilePath;
        public string pngFilePath;
        public float top, bottom, left, right;*/
        public StreamWriter sw;

        public ObjModel(Model m, StreamWriter writer)
        {
            Name = m.Name;
            ont_v_count = m.ont_v_count;
            objFilePath = m.objFilePath;
            pngFilePath = m.pngFilePath;
            top = m.top;
            bottom = m.bottom;
            left = m.left;
            right = m.right;
            sw = writer;
            sw.WriteLine("mtllib " + m.Name + ".mtl");
            writer.WriteLine("usemtl mat0");
        }

        public void Finalize()
        {
            sw.Close();
        }

        public override void AddTriad(int a, int b, int c)
        {
            sw.WriteLine("f {0}/{0} {1}/{1} {2}/{2}",
                    a + 1,
                    b + 1,
                    c + 1);
        }

        public override void AddVertex(float x, float y, float z)
        {
            sw.WriteLine("v {0} {1} {2}",
                    x.ToString(CultureInfo.InvariantCulture),
                    y.ToString(CultureInfo.InvariantCulture),
                    z.ToString(CultureInfo.InvariantCulture));

            float h = (x - left) / (right - left);
            float w = (y - bottom) / (top - bottom);
            sw.WriteLine("vt {0} {1}",
                 h.ToString(CultureInfo.InvariantCulture),
                 w.ToString(CultureInfo.InvariantCulture));
        }
    }
}
