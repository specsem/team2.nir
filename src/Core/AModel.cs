﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public abstract class AModel
    {
        public string Name;
        public int ont_v_count;
        public string objFilePath;
        public string pngFilePath;
        public float top, bottom, left, right;

        abstract public void AddTriad(int a, int b, int c);
        abstract public void AddVertex(float x, float y, float z);
    }
}
