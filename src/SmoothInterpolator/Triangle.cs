﻿using System.Collections.Generic;
using PreProcessing;
using Core;
using System;

namespace SmoothInterpolator
{
    public class Triangle : IInterpolator
    {
        public float xmax { get; } // must be created in constructor;
        public float xmin { get; }
        public float ymax { get; }
        public float ymin { get; }
        private Core.Vertex[] v; //a,b,c

        public Triangle[] T; // T[0] is a neighbour of v[1] and v[2], and so on; // A,B,C

        private IInterpolator I;

        public bool NotTaken;
        public int Step;

        private PlaneInterpolator P;
        private PlaneInterpolator e_ab, e_bc, e_ca; // edges

        //private float ab_a, ab_b, ab_c, ac_a, ac_b, ac_c, bc_a, bc_b, bc_c; // coefficients in edge equations



        float Sign(float[] p1, Core.Vertex p2, Core.Vertex p3) // maybe take numbers, not vertices?
        {
            return (p1[0] - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1[1] - p3.y);
        }
        public bool Contains(float[] coord) // coord[0] = x, coord[1] = y; // can be made way more efficient if first checked for bounding box, and in many other ways
        {
            /*bool b1, b2, b3;
            b1 = Sign(coord, v[0], v[1]) < 0;
            b2 = Sign(coord, v[1], v[2]) < 0;
            b3 = Sign(coord, v[2], v[0]) < 0;
            return ((b1 == b2) && (b2 == b3));*/

            if (e_ab.F(coord[0], coord[1]) < Const.small_float) return false;
            if (e_bc.F(coord[0], coord[1]) < Const.small_float) return false;
            if (e_ca.F(coord[0], coord[1]) < Const.small_float) return false;
            return true;
        }

        public float F(float x, float y)
        {
            /*if (I != null)*/ return I.F(x, y);
            //else return -10; // delete if/else when stable and verified;
        }

        public Triangle(Triad t)
        {
            v = new Core.Vertex[3];
            v[0] = t.ab.A;
            v[1] = t.bc.A;
            v[2] = t.ca.A;
            xmin = v[0].x;
            xmax = v[0].x;
            ymin = v[0].y;
            ymax = v[0].y;
            for (int i = 1; i < 3; i++)
            {
                if (v[i].x > xmax) xmax = v[i].x;
                if (v[i].x < xmin) xmin = v[i].x;
                if (v[i].y > xmax) ymax = v[i].y;
                if (v[i].y < xmin) ymin = v[i].y;
            }
            NotTaken = true;
            T = new Triangle[3];
            Step = 0;
            e_ab = Edge(0, 1, 2);
            e_bc = Edge(1, 2, 0);
            e_ca = Edge(2, 0, 1);
        }

        public void Init(Triad t, List<Triangle> tArray)
        {
            if (t.bc.Belongs.Count == 1) T[0] = null;
            else if (t.bc.Belongs[0] == t) T[0] = tArray[t.bc.Belongs[1].num];
            else T[0] = tArray[t.bc.Belongs[0].num];

            if (t.ca.Belongs.Count == 1) T[1] = null;
            else if (t.ca.Belongs[0] == t) T[1] = tArray[t.ca.Belongs[1].num];
            else T[1] = tArray[t.ca.Belongs[0].num];

            if (t.ab.Belongs.Count == 1) T[2] = null;
            else if (t.ab.Belongs[0] == t) T[2] = tArray[t.ab.Belongs[1].num];
            else T[2] = tArray[t.ab.Belongs[0].num];
        }


        public bool ToTheLeftFrom(float border)
        {
            for (int i = 0; i < 3; i++)
            {
                if (v[i].x > border) return false;
            }
            return true;
        }
        public bool ToTheRightFrom(float border)
        {
            for (int i = 0; i < 3; i++)
            {
                if (v[i].x < border) return false;
            }
            return true;
        }
        public bool LowerThan(float border)
        {
            for (int i = 0; i < 3; i++)
            {
                if (v[i].y > border) return false;
            }
            return true;
        }
        public bool HigherThan(float border)
        {
            for (int i = 0; i < 3; i++)
            {
                if (v[i].y < border) return false;
            }
            return true;
        }

        private PlaneInterpolator GetNeighbourPlane(int p, int p1, int p2)
        {
            if (T[p] == null)
            {
                if (Math.Abs(v[p1].x - v[p2].x) > Math.Abs(v[p1].y - v[p2].y))
                {
                    LineXZ(p1, p2, out float a, out float b, out float c);
                    return new PlaneInterpolator(a / b, 0, c / b);
                }
                else
                {
                    LineYZ(p1, p2, out float a, out float b, out float c);
                    return new PlaneInterpolator(0, a / b, c / b);
                }
            }
            else return T[p].P.Copy();
        }

        public void Interpolate() 
        {
            PlaneInterpolator A, B, C;
            A = GetNeighbourPlane(0, 1, 2);
            B = GetNeighbourPlane(1, 2, 0);
            C = GetNeighbourPlane(2, 0, 1);
            I = new TriplePlaneInterpolator(A, B, C, e_ab, e_bc, e_ca);
        }

        private PlaneInterpolator Edge(int p1, int p2, int p)
        {
            float a = v[p1].y - v[p2].y;
            float b = v[p2].x - v[p1].x;
            float c = v[p1].x * v[p2].y - v[p2].x * v[p1].y;
            if (a * v[p].x + b * v[p].y + c < 0)
            {
                a *= -1;
                b *= -1;
                c *= -1;
            }
            return new PlaneInterpolator(a, b, c);
        }

        private void Line (int p1, int p2, out float a, out float b, out float c, int p)
        {
            a = v[p1].y - v[p2].y;
            b = v[p2].x - v[p1].x;
            c = v[p1].x * v[p2].y - v[p2].x * v[p1].y;
            if (a * v[p].x + b*v[p].y + c < 0)
            {
                a *= -1;
                b *= -1;
                c *= -1;
            }
        }

        private void LineXZ(int p1, int p2, out float a, out float b, out float c)
        {
            a = v[p1].z - v[p2].z;
            b = v[p1].x - v[p2].x;//reversed
            c = v[p1].x * v[p2].z - v[p2].x * v[p1].z;
        }

        private void LineYZ(int p1, int p2, out float a, out float b, out float c)
        {
            a = v[p1].z - v[p2].z;
            b = v[p1].y - v[p2].y;//reversed
            c = v[p1].y * v[p2].z - v[p2].y * v[p1].z;
        }

        public void InterpolateQuadratically(int p) // p1, p2 - line; p - point to reach
        {
            /*int p1, p2;
            if (p != 0) p1 = 0;
            else p1 = 1;
            if (p != 2) p2 = 2;
            else p2 = 1;

            Line(p1, p2, out float a, out float b, out float c, p);
            float z = a * v[p].x + b * v[p].y + c;
            float d = (v[p].z - T[p].I.F(v[p].x, v[p].y)) / (z * z);
            I = T[p].I.Add(d * a * a, 2 * d * a * b, d * b * b, 2 * d * a * c, 2 * d * b * c, d * c * c);*/
            throw new NotImplementedException();
        }

        public void InterpolateLinearly()
        {
            float a, b, c;
            float dx2, dx3, dy2, dy3, dz2, dz3;
            dx2 = v[1].x - v[0].x;
            dx3 = v[2].x - v[0].x;
            dy2 = v[1].y - v[0].y;
            dy3 = v[2].y - v[0].y;
            dz2 = v[1].z - v[0].z;
            dz3 = v[2].z - v[0].z;
            float det = dx2 * dy3 - dy2 * dx3;
            float adet = dz2 * dy3 - dy2 * dz3;
            float bdet = dx2 * dz3 - dz2 * dx3;
            a = adet / det;
            b = bdet / det;
            c = v[0].z - a * v[0].x - b * v[0].y;
            I = new SimpleQuadraticInterpolator(0, 0, 0, a, b, c);
            NotTaken = false;
        }

        public void SetPlaneInterpolator()
        {
            float a, b, c;
            float dx2, dx3, dy2, dy3, dz2, dz3;
            dx2 = v[1].x - v[0].x;
            dx3 = v[2].x - v[0].x;
            dy2 = v[1].y - v[0].y;
            dy3 = v[2].y - v[0].y;
            dz2 = v[1].z - v[0].z;
            dz3 = v[2].z - v[0].z;
            float det = dx2 * dy3 - dy2 * dx3;
            float adet = dz2 * dy3 - dy2 * dz3;
            float bdet = dx2 * dz3 - dz2 * dx3;
            a = adet / det;
            b = bdet / det;
            c = v[0].z - a * v[0].x - b * v[0].y;
            P = new PlaneInterpolator(a, b, c);
        }

        public void InterpolateThreefold()
        {
            I = new TripleInterpolator(T[0].I, T[1].I, T[2].I, e_ab, e_bc, e_ca); // было 0,1,2
        }

        public void InterpolateTwofold() // simplest approach
        {
            IInterpolator[] TI = new IInterpolator[3];
            for (int i = 0; i < 3; i++)
            {
                if (T[i].NonInterpolated())
                {
                    List<int> p = new List<int>(2); // do in a non-retarded way
                    for (int j = 0; j < 3; j++)
                    {
                        if (j != i) p.Add(j);
                    }
                    LineXZ(p[0], p[1], out float a, out float b, out float c);
                    TI[i] = new SimpleQuadraticInterpolator(0, 0, 0, a / b, 0, c / b);
                }
                else
                {
                    TI[i] = T[i].I;
                }
            }
            I = new TripleInterpolator(TI[0], TI[1], TI[2], e_ab, e_bc, e_ca);
        }

        private bool NonInterpolated()
        {
            return (I == null);
        }

        public void AddNonInterpolated(List<Triangle> list)
        {
            if (Step >= Const.I_depth) return;
            for (int i = 0; i < 3; i++)
            {
                if (T[i] != null)
                    if (T[i].NonInterpolated())                    
                        if (T[i].NotTaken)
                        {
                            T[i].Step = Step + 1;
                            list.Add(T[i]);
                            T[i].NotTaken = false;
                        }
            }
        }

        public bool OneSideInterpolated(out int side)
        {
            int c = 0;
            side = -1;//needed?
            for (int i = 0; i < 3; i++)
            {
                if (!NonInterpolated(i))
                {
                    c++;
                    side = i;
                }
            }
            return (c == 1);
        }

        public bool ThreeSidesInterpolated()
        {
            for (int i = 0; i < 3; i++)
            {
                if (NonInterpolated(i))
                {
                    return false;
                }
            }
            return true;
        }

        public bool ZeroSidesInterpolated()
        {
            for (int i = 0; i < 3; i++)
            {
                if (!NonInterpolated(i))
                {
                    return false;
                }
            }
            return true;
        }

        private bool NonInterpolated (int side)
        {
            if (T[side] == null) return true;
            return T[side].NonInterpolated();
        }
    }
}
