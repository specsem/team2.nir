﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace SmoothInterpolator
{
    /*

    public class IEdge
    {
        public List<Triad> Belongs;
        public Vertex A;
        public Vertex B;
        public IEdge(Vertex a, Vertex b)
        {
            A = a;
            B = b;
            Belongs = new List<Triad>();
        }
    }
    public class Triad
    {
        public IEdge ab, bc, ca;
        public int num;
        

        public Triad(IEdge ab, IEdge bc, IEdge ca)
        {
            this.ab = ab;
            this.bc = bc;
            this.ca = ca;

            ab.Belongs.Add(this);
            bc.Belongs.Add(this);
            ca.Belongs.Add(this);
        }
    }

    public static class Test
    {
        public static List<Triad> Pyramid()
        {
            Vertex p, p1, p2, p3, p4, p5, p6, p7, p8;
            p = new Vertex(0, 0, 1);
            p1 = new Vertex(1, 0, 0);
            p2 = new Vertex(0, 1, 0);
            p3 = new Vertex(-1, 0, 0);
            p4 = new Vertex(0, -1, 0);
            p5 = new Vertex(1, 1, 0);
            p6 = new Vertex(-1, 1, 0);
            p7 = new Vertex(-1, -1, 0);
            p8 = new Vertex(1, -1, 0);

            IEdge A1, A2, A3, A4, B1, B2, B3, B4, C1, C2, C3, C4, C5, C6, C7, C8;

            A1 = new IEdge(p,p1);
            A2 = new IEdge(p2,p);
            A3 = new IEdge(p,p3);
            A4 = new IEdge(p4,p);

            B1 = new IEdge(p1, p2);
            B2 = new IEdge(p3, p2);
            B3 = new IEdge(p3, p4);
            B4 = new IEdge(p1, p4);

            C1 = new IEdge(p2, p5);
            C2 = new IEdge(p2, p6);
            C3 = new IEdge(p6, p3);
            C4 = new IEdge(p7, p3);
            C5 = new IEdge(p4, p7);
            C6 = new IEdge(p4, p8);
            C7 = new IEdge(p8, p1);
            C8 = new IEdge(p5, p1);

            List<Triad> list = new List<Triad>();

            list.Add(new Triad(A1, B1, A2));
            list.Add(new Triad(A3, B2, A2));
            list.Add(new Triad(A3, B3, A4));
            list.Add(new Triad(A1, B4, A4));
            list.Add(new Triad(B1, C1, C8));
            list.Add(new Triad(B2, C2, C3));
            list.Add(new Triad(B3, C5, C4));
            list.Add(new Triad(B4, C6, C7));

            return list;
        }
    }

    */
}
