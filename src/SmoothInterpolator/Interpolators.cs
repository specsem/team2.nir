﻿using System;
using PreProcessing;
using System.Collections.Generic;

namespace SmoothInterpolator
{
    public class AverageInterpolator : IInterpolator // can be redone much more efficiently
    {
        private List<IInterpolator> list;

        public AverageInterpolator(List<IInterpolator> list)
        {
            this.list = list;
        }

        public float F(float x, float y)
        {
            float f = 0;
            foreach(IInterpolator I in list)
            {
                f += I.F(x, y);
            }
            f /= list.Count;
            return f;
        }
    }

    public interface ISInterpolator : IInterpolator
    {
        SimpleQuadraticInterpolator Add(float da, float db, float dc, float dd, float de, float df);
    }    

    public class SimpleQuadraticInterpolator : ISInterpolator // 5+ 8*
    {
        private float a, b, c, d, e, f;

        public SimpleQuadraticInterpolator(float a, float b, float c, float d, float e, float f)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
        }

        public SimpleQuadraticInterpolator Add(float da, float db, float dc, float dd, float de, float df)
        {
            return new SimpleQuadraticInterpolator(a + da, b + db, c + dc, d + dd, e + de, f + df);
        }

        /*public EfficientQuadraticInterpolator MakeEfficient() //validated
        {
            float B, C, D, E, F;
            B = b / (2 * a);
            C = d / (2 * a);
            D = c - a * B * B;
            E = (e - 2 * a * B * C) / (2 * D);
            F = f - a * C * C - D * E * E;
            return new EfficientQuadraticInterpolator(a, B, C, D, E, F);
        }*/

        public float F(float x, float y)
        {
            return a * x * x + b * x * y + c * y * y + d * x + e * y + f;
        }
    }

    public class PlaneInterpolator : IInterpolator 
    {
        private float a, b, c;

        public PlaneInterpolator(float a, float b, float c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }        

        public float F(float x, float y)
        {
            return a * x + b * y + c;
        }

        public void Mult(float k)
        {
            a *= k;
            b *= k;
            c *= k;
        }

        public void Deduct(PlaneInterpolator P)
        {
            a -= P.a;
            b -= P.b;
            c -= P.c;
        }

        public void Sum(PlaneInterpolator P)
        {
            a += P.a;
            b += P.b;
            c += P.c;
        }

        public PlaneInterpolator Copy()
        {
            return new PlaneInterpolator(a, b, c);
        }
    }

    /*public class EfficientQuadraticInterpolator : ISInterpolator // 5+ 5* //not actually efficient;
    {
        private float a, b, c, d, e, f;

        public EfficientQuadraticInterpolator (float a, float b, float c, float d, float e, float f)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
        }

        public SimpleQuadraticInterpolator Add(float da, float db, float dc, float dd, float de, float df)
        {
            throw new NotImplementedException();
        }

        public float F(float x, float y)
        {
            float g = (x + b * y + c);
            float h = (y + e);
            return a * g * g + d * h * h + f;

            //return (float)(a * Math.Pow((x + b * y + c), 2) + d * Math.Pow((y + e), 2) + f);
        }
    }*/

    public class TripleInterpolator : ISInterpolator // maybe accept input as array?
    {
        private IInterpolator A, B, C;
        private PlaneInterpolator e_ab, e_bc, e_ca;

        public TripleInterpolator(IInterpolator a, IInterpolator b, IInterpolator c, PlaneInterpolator e_ab, PlaneInterpolator e_bc, PlaneInterpolator e_ca)
        {
            A = a;
            B = b;
            C = c;
            this.e_ab = e_ab;
            this.e_bc = e_bc;
            this.e_ca = e_ca;
        }

        public float F(float x, float y)
        {
            float a, b, c, ab, ac, bc;
            ab = e_ab.F(x, y);
            ab *= ab;
            ac = e_ca.F(x, y);
            ac *= ac;
            bc = e_bc.F(x, y);
            bc *= bc;
            a = ab * ac;
            b = ab * bc;
            c = ac * bc;
            return (A.F(x, y) * a + B.F(x, y) * b + C.F(x, y) * c) / (a + b + c);
        }

        public SimpleQuadraticInterpolator Add(float da, float db, float dc, float dd, float de, float df)
        {
            throw new NotImplementedException();
        }
    }

    public class TriplePlaneInterpolator : IInterpolator 
    {
        private PlaneInterpolator A, B, C;
        private PlaneInterpolator e_ab, e_bc, e_ca;

        public TriplePlaneInterpolator(PlaneInterpolator a, PlaneInterpolator b, PlaneInterpolator c, PlaneInterpolator e_ab, PlaneInterpolator e_bc, PlaneInterpolator e_ca)
        {
            A = a;
            B = b;
            C = c;
            B.Deduct(A);
            C.Deduct(A);
            this.e_ab = e_ab;
            this.e_bc = e_bc;
            this.e_ca = e_ca;
        }        

        public float F(float x, float y)
        {
            float a, b, c, ab, ac, bc;
            ab = e_ab.F(x, y);
            ab *= ab;
            ac = e_ca.F(x, y);
            ac *= ac;
            bc = e_bc.F(x, y);
            bc *= bc;
            a = ab * ac;
            b = ab * bc;
            c = ac * bc;
            return A.F(x, y) + (B.F(x, y) * b + C.F(x, y) * c) / (a + b + c);
        }

        public void Average(PlaneInterpolator P)
        {
            A.Sum(P);
            A.Mult(0.5f);
            B.Mult(0.5f);
            C.Mult(0.5f);
        }
    }
}



/*
Random rnd = new Random();

            while (true)
            {
                SimpleQuadraticInterpolator I = new SimpleQuadraticInterpolator((float)rnd.NextDouble(), (float)rnd.NextDouble(), (float)rnd.NextDouble(), (float)rnd.NextDouble(), (float)rnd.NextDouble(), (float)rnd.NextDouble());
EfficientQuadraticInterpolator E = I.MakeEfficient();

Stopwatch watch1 = new Stopwatch();
watch1.Start();
                for (int i = 0; i< 1000000; i++)
                {
                    float dif = I.F(2.3f, 1.4f);
                }
                watch1.Stop();
                Console.WriteLine("Simple time is " + watch1.ElapsedMilliseconds + " ms");

                Stopwatch watch2 = new Stopwatch();
watch2.Start();
                for (int i = 0; i< 1000000; i++)
                {
                    float dif = E.F(2.3f, 1.4f);
                }
                watch2.Stop();
                Console.WriteLine("Efficient time is " + watch2.ElapsedMilliseconds + " ms");

                Console.ReadLine();
            }

    */