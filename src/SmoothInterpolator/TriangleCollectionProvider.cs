﻿using System;
using System.Collections.Generic;

namespace SmoothInterpolator
{
    public abstract class ITriangleCollection
    {
        public abstract Triangle Find(float[] coord); // coord[0] = x, coord[1] = y;
        public Triangle Find(float x, float y)
        {
            return Find(new float[2] { x, y });
        }
    }

    public class TriangleBranch : ITriangleCollection
    {
        private Triangle[] T;

        public TriangleBranch(List<Triangle> list)
        {
            T = list.ToArray();
        }

        public override Triangle Find(float[] coord)
        {
            for (int i = 0; i < T.Length; i++)
            {
                if (T[i].Contains(coord)) return T[i];
            }
            return null;
        }
    }

    public class TriangleTree : ITriangleCollection
    {
        private ITriangleCollection[] T; // две коллекции - левая и правая
        private ITriangleCollection M; // центральная коллекция
        private int mode; // 0 - x, 1 - y
        private float border;

        public TriangleTree(ITriangleCollection T0, ITriangleCollection T1, ITriangleCollection TM, float border, int mode)
        {
            this.mode = mode;
            this.border = border;
            M = TM;
            T = new ITriangleCollection[2] { T0, T1 };
        }

        public override Triangle Find(float[] coord)
        {
            Triangle result = T[Convert.ToInt32(coord[mode] > border)].Find(coord);
            if (result == null) result = M.Find(coord);
            return result;
        }
    }

    public static class TriangleCollectionProvider
    {
        public static ITriangleCollection Generate(List<Triangle> list) // static border generation. Can do dynamic, will make
        {                                                               // construction heavier and maybe make interpolation lighter
            if (list.Count < 4) return new TriangleBranch(list);
            float xmin = list[0].xmin;
            float xmax = list[0].xmax;
            float ymin = list[0].ymin;
            float ymax = list[0].ymax;
            for (int i = 1; i < list.Count; i++)
            {
                if (list[i].xmax > xmax) xmax = list[i].xmax;
                if (list[i].xmin < xmin) xmin = list[i].xmin;
                if (list[i].ymax > xmax) ymax = list[i].ymax;
                if (list[i].ymin < xmin) ymin = list[i].ymin;
            }
            float border;
            List<Triangle> T0 = new List<Triangle>();
            List<Triangle> T1 = new List<Triangle>();
            List<Triangle> TM = new List<Triangle>();
            bool ymod = (xmax - xmin < ymax - ymin);
            int mode = Convert.ToInt32(ymod); // 0 - x, 1 - y
            if (ymod)
            {
                border = (ymax + ymin) / 2;
                foreach (Triangle tri in list)
                {
                    if (tri.LowerThan(border)) T0.Add(tri);
                    else if (tri.HigherThan(border)) T1.Add(tri);
                    else TM.Add(tri);
                }
            }
            else
            {
                border = (xmax + xmin) / 2;
                foreach (Triangle tri in list)
                {
                    if (tri.ToTheLeftFrom(border)) T0.Add(tri);
                    else if (tri.ToTheRightFrom(border)) T1.Add(tri);
                    else TM.Add(tri);
                }
            }
            if ((float)TM.Count / list.Count < Const.divide_effectiveness) // probably wrong measure
            {
                return new TriangleTree(Generate(T0), Generate(T1), Generate(TM), border, mode);
            }
            else
            {
                return new TriangleBranch(list);
            }
        }

    }    
}
