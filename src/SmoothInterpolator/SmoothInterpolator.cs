﻿using System;
using System.Collections.Generic;
using PreProcessing;
using Core;


namespace SmoothInterpolator
{
    public static class Const
    {
        public const float divide_effectiveness = 0.3f; //mayby another way of measuring that?
        public const float small_float = 0.0f;
        public const int I_depth = 2;
    }

          

    public class SmoothInterpolator : IInterpolator //TODO: Add condition that if a point to interpolate was in input, it is given right away
    {                                               // probably that's why it gives you NaN - a bug to correct
        private ITriangleCollection tTree;
        private List<Triangle> tArray; // или всё же массив? определённо массив, на крайняк - и массив, и лист
        //private int param;
        private Random rnd;

        public SmoothInterpolator(Model m) // divide in constr and Init for AverageInterpolator
        {
            List<Triad> list = m.tList;
            tArray = new List<Triangle>(list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                list[i].num = i;
                tArray.Add(new Triangle(list[i]));
            }
            for (int i = 0; i < list.Count; i++)
            {
                tArray[i].Init(list[i], tArray);
            }
            tTree = TriangleCollectionProvider.Generate(tArray);

            NewInit();
        }

        private void NewInit()
        {
            for (int i = 0; i < tArray.Count; i++)
            {
                tArray[i].SetPlaneInterpolator();
            }
            for (int i = 0; i < tArray.Count; i++)
            {
                tArray[i].Interpolate();
            }
        }

        private void OldInit(List<Triad> list, Random rnd) // OBSOLETE?
        {
            this.rnd = rnd;
            List<Triangle> t3List = new List<Triangle>();
            int first = ChooseFirst(list.Count);
            Triangle t = tArray[first];
            int iter = 0;
            bool cont = true;
            while (cont)
            {
                t.InterpolateLinearly();
                List<Triangle> tList = new List<Triangle>();
                t.AddNonInterpolated(tList);
                int j = 0;
                while (tList.Count > j)
                {
                    if (tList[j].OneSideInterpolated(out int side))
                    {
                        tList[j].InterpolateQuadratically(side);
                        tList[j].AddNonInterpolated(tList);
                    }
                    else t3List.Add(tList[j]);
                    j++;
                }
                cont = false; // move to while
                for (; iter < tArray.Count; iter++) // optimize i
                {
                    if ((tArray[iter].NotTaken) && (tArray[iter].ZeroSidesInterpolated()))
                    {
                        t = tArray[iter];
                        cont = true;
                        break;
                    }
                }
            }

            for (int i = 0; i < t3List.Count; i++)
            {
                if (t3List[i].ThreeSidesInterpolated()) t3List[i].InterpolateThreefold();
                else
                {
                    t3List[i].InterpolateTwofold();
                }
            }

            List<Triangle> rest = new List<Triangle>();
            for (int i = 0; i < tArray.Count; i++)
            {
                if (tArray[i].NotTaken)
                {
                    rest.Add(tArray[i]);
                }
            }
            List<Triangle> rest2 = new List<Triangle>();
            for (int i = 0; i < rest.Count; i++)
            {
                if (rest[i].OneSideInterpolated(out int side))
                {
                    rest[i].InterpolateQuadratically(side);
                }
                else
                {
                    rest2.Add(rest[i]);
                }
            }
            for (int i = 0; i < rest2.Count; i++)
            {
                if (rest2[i].ThreeSidesInterpolated())
                {
                    rest2[i].InterpolateThreefold();
                }
                else
                {
                    rest2[i].InterpolateTwofold();
                }
            }
        }

        private int ChooseFirst(int n) // maybe other variants?
        {
            return rnd.Next(n);
        }

        public float F(float x, float y)
        {            
            Triangle T = tTree.Find(x, y);
            if (T == null) // delete later?
            {
                //Console.WriteLine("ERROR: SmoothInterpolator, no triangle found");
                //Console.WriteLine(x + ", " + y);
                //Console.ReadLine();
                return 0;
            }
            else
            {
                //
                float result = T.F(x, y);
                if (result != result)
                {
                    Console.WriteLine("SmoothInterpolator returned NaN");
                    result = 0;
                }
                else if (float.IsInfinity(result))
                {
                    Console.WriteLine("SmoothInterpolator returned Infinity");
                    result = 0;
                }
                //
                return result;
            }
        }
    }

    
}
