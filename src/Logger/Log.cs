﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    static public class Log
    {
        static public void Error(string message,
    [CallerLineNumber] int lineNumber = 0,
    [CallerMemberName] string caller = null)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[E]" + " (" + caller + ":" + lineNumber + ")" + message);
            Console.ResetColor();
        }

        static public void Warning(string message,
    [CallerLineNumber] int lineNumber = 0,
    [CallerMemberName] string caller = null)
        {
            Console.WriteLine(message + " at line " + lineNumber + " (" + caller + ")");
        }

    }
}
