﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Globalization;

/*
  copyright s-hull.org 2011
  released under the contributors beerware license

  contributors: Phil Atkin, Dr Sinclair.
*/
namespace DelaunayTriangulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Random randy = new Random(138);

            List<Vertex> points = new List<Vertex>();

            if (args.Length == 0)
            {
                // Generate random points.
                SortedDictionary<int, Point> ps = new SortedDictionary<int, Point>();
                for (int i = 0; i < 100000; i++)
                {
                    int x = randy.Next(100000);
                    int y = randy.Next(100000);
                    points.Add(new Vertex(x, y));
                }
            }
            else
            {
                // Read a points file as used by the Delaunay Triangulation Tester program DTT
                // (See http://gemma.uni-mb.si/dtt/)
                using (StreamReader reader = new StreamReader(args[0]))
                {
                    //int count = int.Parse(reader.ReadLine());
                    //for (int i = 0; i < count; i++)
                    //{
                    //    string line = reader.ReadLine();
                    //    string[] split = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    //    points.Add(new Vertex(float.Parse(split[0]), float.Parse(split[1])));
                    //}
                    string data = reader.ReadToEnd();
                    string[] a_strs = data.Split('\n');

                    foreach (string s in a_strs)
                    {
                        if (s.StartsWith("#"))
                            continue;
                        if (s.StartsWith("v "))
                        {
                            //thats a dot
                            string[] a_coords = s.Split();
                            float d1 = (float)Double.Parse(a_coords[1], CultureInfo.InvariantCulture);
                            float d2 = (float)Double.Parse(a_coords[2], CultureInfo.InvariantCulture);
                            float d3 = (float)Double.Parse(a_coords[3], CultureInfo.InvariantCulture);
                            points.Add(new Vertex(d1, d2, d3));
                        }
                        else
                        {
                            if (s.Trim().Length != 0)
                                Console.WriteLine("Unknown string in obj: {0}", s);
                        }
                    }
                    Console.WriteLine("Parse complete. Verteces: {0}", points.Count);
                }
            }

            // Write the points in the format suitable for DTT
            //using (StreamWriter writer = new StreamWriter("Triangulation c#.pnt"))
            //{
            //    writer.WriteLine(points.Count);
            //    for (int i = 0; i < points.Count; i++)
            //        writer.WriteLine(String.Format("{0},{1}", points[i].x, points[i].y));
            //}

            // Write out the data set we're actually going to triangulate
            Triangulator angulator = new Triangulator();

            Stopwatch watch = new Stopwatch();
            watch.Start();
            List<Triad> triangles = angulator.Triangulation(points, true);
            watch.Stop();

            Console.WriteLine(watch.ElapsedMilliseconds + " ms");

            // Write the triangulation results
            using (StreamWriter writer = new StreamWriter(Path.GetFileNameWithoutExtension(args[0]) + "_triang.obj"))
            {
                //writer.WriteLine(triangles.Count.ToString());
                //for (int i = 0; i < triangles.Count; i++)
                //{
                //    Triad t = triangles[i];
                //    writer.WriteLine(string.Format("{0}: {1} {2} {3} - {4} {5} {6}",
                //        i + 1,
                //        t.a, t.b, t.c,
                //        t.ab + 1, t.bc + 1, t.ac + 1));
                //}

                writer.WriteLine("mtllib " + Path.GetFileNameWithoutExtension(args[0]) + "_triang.mtl");

                // calculating max top, bottom, left, right values
                float top = points[0].y, bottom = points[0].y, left = points[0].x, right = points[0].x;
                for (int i = 0; i < points.Count; i++)
                {
                    if (points[i].y > top)
                        top = points[i].y;
                    else if (points[i].y < bottom)
                        bottom = points[i].y;
                    if (points[i].x > right)
                        right = points[i].x;
                    else if (points[i].x < left)
                        left = points[i].x;
                }
                for (int i = 0; i < points.Count; i++)
                {
                    writer.WriteLine("v {0} {1} {2}",
                        points[i].x.ToString(CultureInfo.InvariantCulture),
                        points[i].y.ToString(CultureInfo.InvariantCulture),
                        points[i].z.ToString(CultureInfo.InvariantCulture));
                }
                for (int i = 0; i < points.Count; i++)
                {
                    float h = (points[i].x - left) / (right - left);
                    float w = (points[i].y - bottom) / (top - bottom);
                    writer.WriteLine("vt {0} {1}",
                         h.ToString(CultureInfo.InvariantCulture),
                         w.ToString(CultureInfo.InvariantCulture));
                }
                writer.WriteLine("usemtl mat0");
                for (int i = 0; i < triangles.Count; i++)
                {
                    writer.WriteLine("f {0}/{0} {1}/{1} {2}/{2}",
                        triangles[i].a + 1,
                        triangles[i].b + 1,
                        triangles[i].c + 1);
                }
            }

            using (StreamWriter writer = new StreamWriter(Path.GetFileNameWithoutExtension(args[0]) + "_triang.mtl"))
            {
                writer.WriteLine("newmtl mat0");
                writer.WriteLine("map_Kd " + Path.GetFileName(args[1]));
            }
        }
    }
}
